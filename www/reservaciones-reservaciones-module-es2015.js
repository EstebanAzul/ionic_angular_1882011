(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["reservaciones-reservaciones-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/reservaciones/reservaciones.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/reservaciones/reservaciones.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button menu=\"menu1\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Reservaciones</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size-md=\"6\" offset-md=\"3\" class=\"ion-text-center\" *ngIf=\"isLoading\">\n        <ion-spinner color=\"primary\"></ion-spinner>\n      </ion-col>\n      <ion-col size-md=\"6\" offset-md=\"3\" *ngIf=\"!isLoading && (!reservacionesCargadas || reservacionesCargadas.length <= 0)\" class=\"ion-text-center\">\n        <p>No hay reservaciones</p>\n      </ion-col>\n      <ion-col size-md=\"6\" offset-md=\"3\" *ngIf=\"!isLoading && (reservacionesCargadas.length > 0)\">\n          <ion-list>\n            <ion-item-sliding *ngFor=\"let rsv of reservacionesCargadas\" #slidingRsv>\n              <ion-item>\n                <ion-avatar slot=\"start\"><ion-img [src]=\"rsv.imageUrl\"></ion-img></ion-avatar>\n                <ion-label>\n                  <h2>{{rsv.lugarTitulo}}</h2>\n                  <p>Huespedes: {{rsv.huespedes}}</p>\n                </ion-label>\n              </ion-item>\n              <ion-item-options side=\"start\">\n                <ion-item-option color=\"danger\" (click)=\"onRemoveReservacion(rsv.firebaseId, slidingRsv)\">\n                  <ion-icon name=\"trash\" slot=\"icon-only\"></ion-icon>\n                </ion-item-option>\n              </ion-item-options>\n            </ion-item-sliding>\n          </ion-list>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>");

/***/ }),

/***/ "./src/app/reservaciones/reservacion.model.ts":
/*!****************************************************!*\
  !*** ./src/app/reservaciones/reservacion.model.ts ***!
  \****************************************************/
/*! exports provided: Reservacion */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Reservacion", function() { return Reservacion; });
class Reservacion {
    constructor(id, lugarId, usuarioId, lugarTitulo, imageUrl, nombre, apellido, huespedes, desde, hasta, firebaseId) {
        this.id = id;
        this.lugarId = lugarId;
        this.usuarioId = usuarioId;
        this.lugarTitulo = lugarTitulo;
        this.imageUrl = imageUrl;
        this.nombre = nombre;
        this.apellido = apellido;
        this.huespedes = huespedes;
        this.desde = desde;
        this.hasta = hasta;
        this.firebaseId = firebaseId;
    }
}


/***/ }),

/***/ "./src/app/reservaciones/reservacion.service.ts":
/*!******************************************************!*\
  !*** ./src/app/reservaciones/reservacion.service.ts ***!
  \******************************************************/
/*! exports provided: ReservacionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReservacionService", function() { return ReservacionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _login_login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../login/login.service */ "./src/app/login/login.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _reservacion_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./reservacion.model */ "./src/app/reservaciones/reservacion.model.ts");







let ReservacionService = class ReservacionService {
    constructor(loginService, http) {
        this.loginService = loginService;
        this.http = http;
        this._reservaciones = new rxjs__WEBPACK_IMPORTED_MODULE_5__["BehaviorSubject"]([]);
    }
    get reservaciones() {
        return this._reservaciones.asObservable();
    }
    fetchReservaciones() {
        let url = 'https://practica10-5b315.firebaseio.com/reservaciones.json?orderBy="usuarioId"&equalTo=${this.loginService.usuarioId}';
        return this.http.get(url).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(rsvDta => {
            const reservaciones = [];
            for (const key in rsvDta) {
                if (rsvDta.hasOwnProperty(key)) {
                    reservaciones.push(new _reservacion_model__WEBPACK_IMPORTED_MODULE_6__["Reservacion"](rsvDta[key].id, rsvDta[key].lugarId, rsvDta[key].usuarioId, rsvDta[key].lugarTitulo, rsvDta[key].imageUrl, rsvDta[key].nombre, rsvDta[key].apellido, rsvDta[key].huespedes, new Date(rsvDta[key].desde), new Date(rsvDta[key].hasta), key));
                }
            }
            return reservaciones;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["tap"])(rsvs => {
            this._reservaciones.next(rsvs);
        }));
    }
    addReservacion(lugarId, descripcion, imageUrl, nombre, apellido, huespedes, desde, hasta) {
        let firebaseId;
        const newReservacion = new _reservacion_model__WEBPACK_IMPORTED_MODULE_6__["Reservacion"](Math.random() * 100, lugarId, this.loginService.usuarioId, descripcion, imageUrl, nombre, apellido, huespedes, desde, hasta, null);
        return this.http.post('https://proyecto10-390e5.firebaseio.com/reservaciones.json', Object.assign(Object.assign({}, newReservacion), { firebaseId: null })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["switchMap"])(resData => {
            firebaseId = resData.name;
            return this.reservaciones;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["tap"])(rsvs => {
            this._reservaciones.next(rsvs.concat(newReservacion));
        }));
        /*
        console.log(newReservacion);
        return this.reservaciones.pipe(take(1 ), delay(1000),
        tap(rsvs => {
        this._reservaciones.next(rsvs.concat(newReservacion));
        }));
        */
    }
    cancelarReservacion(firebaseId) {
        return this.http.delete('https://proyecto10-390e5.firebaseio.com//reservaciones/${firebaseId}.json')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["switchMap"])(() => {
            return this.reservaciones;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["tap"])(rsvs => {
            this._reservaciones.next(rsvs.filter(r => {
                r.firebaseId !== firebaseId;
            }));
        }));
    }
};
ReservacionService.ctorParameters = () => [
    { type: _login_login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ReservacionService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Injectable"])({
        providedIn: 'root'
    })
], ReservacionService);



/***/ }),

/***/ "./src/app/reservaciones/reservaciones-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/reservaciones/reservaciones-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: ReservacionesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReservacionesPageRoutingModule", function() { return ReservacionesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _reservaciones_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./reservaciones.page */ "./src/app/reservaciones/reservaciones.page.ts");




const routes = [
    {
        path: '',
        component: _reservaciones_page__WEBPACK_IMPORTED_MODULE_3__["ReservacionesPage"]
    }
];
let ReservacionesPageRoutingModule = class ReservacionesPageRoutingModule {
};
ReservacionesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ReservacionesPageRoutingModule);



/***/ }),

/***/ "./src/app/reservaciones/reservaciones.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/reservaciones/reservaciones.module.ts ***!
  \*******************************************************/
/*! exports provided: ReservacionesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReservacionesPageModule", function() { return ReservacionesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _reservaciones_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./reservaciones-routing.module */ "./src/app/reservaciones/reservaciones-routing.module.ts");
/* harmony import */ var _reservaciones_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./reservaciones.page */ "./src/app/reservaciones/reservaciones.page.ts");







let ReservacionesPageModule = class ReservacionesPageModule {
};
ReservacionesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _reservaciones_routing_module__WEBPACK_IMPORTED_MODULE_5__["ReservacionesPageRoutingModule"]
        ],
        declarations: [_reservaciones_page__WEBPACK_IMPORTED_MODULE_6__["ReservacionesPage"]]
    })
], ReservacionesPageModule);



/***/ }),

/***/ "./src/app/reservaciones/reservaciones.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/reservaciones/reservaciones.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Jlc2VydmFjaW9uZXMvcmVzZXJ2YWNpb25lcy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/reservaciones/reservaciones.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/reservaciones/reservaciones.page.ts ***!
  \*****************************************************/
/*! exports provided: ReservacionesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReservacionesPage", function() { return ReservacionesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _reservacion_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reservacion.service */ "./src/app/reservaciones/reservacion.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");




let ReservacionesPage = class ReservacionesPage {
    constructor(reservacionService, loadingCtrl) {
        this.reservacionService = reservacionService;
        this.loadingCtrl = loadingCtrl;
        this.isLoading = false;
    }
    ngOnInit() {
        this.reservacionService.reservaciones.subscribe(rsvs => {
            this.reservacionesCargadas = rsvs;
        });
    }
    ionViewWillEnter() {
        this.isLoading = true;
        this.reservacionService.fetchReservaciones().subscribe(() => {
            this.isLoading = false;
        });
    }
    ngOnDestroy() {
        if (this.reservacionSub) {
            this.reservacionSub.unsubscribe();
        }
    }
    onRemoveReservacion(firebaseId, slidingEl) {
        slidingEl.close();
        this.loadingCtrl.create({ message: 'cancelando reservacion...' }).then(loadingEl => {
            loadingEl.present();
            this.reservacionService.cancelarReservacion(firebaseId).subscribe(() => {
                loadingEl.dismiss();
            });
        });
    }
};
ReservacionesPage.ctorParameters = () => [
    { type: _reservacion_service__WEBPACK_IMPORTED_MODULE_1__["ReservacionService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] }
];
ReservacionesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-reservaciones',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./reservaciones.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/reservaciones/reservaciones.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./reservaciones.page.scss */ "./src/app/reservaciones/reservaciones.page.scss")).default]
    })
], ReservacionesPage);



/***/ })

}]);
//# sourceMappingURL=reservaciones-reservaciones-module-es2015.js.map