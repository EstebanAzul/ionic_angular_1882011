export interface Coordenadas {
    lat: number;
    lng: number;
}

export interface LugarUbicacion extends Coordenadas {
    adress: string;
    staticMapImageUrl: string;
}