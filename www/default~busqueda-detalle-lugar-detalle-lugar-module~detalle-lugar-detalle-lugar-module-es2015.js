(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~busqueda-detalle-lugar-detalle-lugar-module~detalle-lugar-detalle-lugar-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/busqueda/detalle-lugar/detalle-lugar.page.html":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/busqueda/detalle-lugar/detalle-lugar.page.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/lugares/tabs/busqueda\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Detalle Lugar</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-text-center\">\n  <div *ngIf=\"isLoading\">\n    <ion-spinner color=\"primary\"></ion-spinner>\n  </div>\n  <ion-grid *ngIf=\"!isLoading\">\n    <ion-row>\n    <ion-col size-sm=\"6\" offset-sm=\"3\">\n    <ion-img [src]=\"lugarActual.imageUrl\"></ion-img>\n    </ion-col>\n    </ion-row>\n    <ion-row>\n    <ion-col size-sm=\"6\" offset-sm=\"3\">\n    <p>{{lugarActual.descripcion}}</p>\n    </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\">\n        <p>{{lugarActual.ubicacion.adress}}</p>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size-sm=\"6\" offset-sm=\"3\" class=\"ion-no-padding\">\n        <ion-img role=\"button\" (click)=\"onMostrarMapa()\" [src]=\"lugarActual.ubicacion.staticMapImageUrl\"></ion-img>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n    <ion-col size-sm=\"6\" offset-sm=\"3\" class=\"ion-text-center\">\n    <ion-button color=\"primary\" class=\"ion-margin\" (click)=\"onReservarLugar()\">Reservar</ion-button>\n    </ion-col>\n    </ion-row>\n  </ion-grid>\n  \n\n</ion-content>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/reservaciones/nueva-reservacion/nueva-reservacion.component.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/reservaciones/nueva-reservacion/nueva-reservacion.component.html ***!
  \************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header> \n  <ion-toolbar>\n    <ion-title>Confirmacion</ion-title>\n    <ion-buttons slot=\"primary\">\n      <ion-button (click)=\"onCancel()\">\n      <ion-icon name=\"close\"></ion-icon>  \n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-text-center\">\n  <p>Estas seguro de reservar {{lugar.titulo}} ?</p>\n  <form (ngSubmit)=\"onBookPlace()\" #formNew=\"ngForm\">\n    <ion-grid>\n      <ion-row>\n        <ion-col size-sm=\"6\" offset-sm=\"3\" class=\"ion-text-center\">\n          <ion-img [src]=\"lugar.imageUrl\"></ion-img>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col size-sm=\"6\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Nombre</ion-label>\n            <ion-input type=\"text\" ngModel name=\"nombre\" required></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col size-sm=\"6\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Apellidos</ion-label>\n            <ion-input type=\"text\" ngModel name=\"apellido\" required></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col size-sm=\"6\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Numero de Huespedes</ion-label>\n            <ion-select [ngModel]=\"'2'\" name=\"numero-huespedes\">\n              <ion-select-option value=\"1\">1</ion-select-option>\n              <ion-select-option value=\"2\">2</ion-select-option>\n              <ion-select-option value=\"3\">3</ion-select-option>\n              <ion-select-option value=\"4\">4</ion-select-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col size-sm=\"6\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Desde</ion-label>\n            <ion-datetime display-format=\"MM DD YYYY\" picker-format=\"YY MMM DD\" [min]=\"lugar.disponibleDesde.toISOString()\" [max]=\"lugar.disponibleHasta.toISOString()\" [ngModel]=\"desde\" name=\"fec-desde\" required #desdeCtrl></ion-datetime>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col size-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Hasta</ion-label>\n            <ion-datetime display-format=\"MMM DD YYYY\" picker-format=\"YY MMM DD\" [min]=\"desdeCtrl.value\" [max]=\"lugar.disponibleHasta.toISOString()\" [ngModel]=\"hasta\" name=\"fec-hasta\" required></ion-datetime>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n\n      <ion-row>\n        <ion-col size-sm=\"6\" offset-sm=\"3\">\n          <ion-button color=\"primary\" type=\"submit\" [disabled]=\"!formNew.valid || !fechasValidas()\">Si Reservar</ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/lugares/busqueda/detalle-lugar/detalle-lugar-routing.module.ts":
/*!********************************************************************************!*\
  !*** ./src/app/lugares/busqueda/detalle-lugar/detalle-lugar-routing.module.ts ***!
  \********************************************************************************/
/*! exports provided: DetalleLugarPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalleLugarPageRoutingModule", function() { return DetalleLugarPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _detalle_lugar_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./detalle-lugar.page */ "./src/app/lugares/busqueda/detalle-lugar/detalle-lugar.page.ts");




const routes = [
    {
        path: '',
        component: _detalle_lugar_page__WEBPACK_IMPORTED_MODULE_3__["DetalleLugarPage"]
    }
];
let DetalleLugarPageRoutingModule = class DetalleLugarPageRoutingModule {
};
DetalleLugarPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DetalleLugarPageRoutingModule);



/***/ }),

/***/ "./src/app/lugares/busqueda/detalle-lugar/detalle-lugar.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/lugares/busqueda/detalle-lugar/detalle-lugar.module.ts ***!
  \************************************************************************/
/*! exports provided: DetalleLugarPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalleLugarPageModule", function() { return DetalleLugarPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _reservaciones_nueva_reservacion_nueva_reservacion_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../../reservaciones/nueva-reservacion/nueva-reservacion.component */ "./src/app/reservaciones/nueva-reservacion/nueva-reservacion.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _detalle_lugar_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./detalle-lugar-routing.module */ "./src/app/lugares/busqueda/detalle-lugar/detalle-lugar-routing.module.ts");
/* harmony import */ var _detalle_lugar_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./detalle-lugar.page */ "./src/app/lugares/busqueda/detalle-lugar/detalle-lugar.page.ts");









let DetalleLugarPageModule = class DetalleLugarPageModule {
};
DetalleLugarPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"],
            _detalle_lugar_routing_module__WEBPACK_IMPORTED_MODULE_7__["DetalleLugarPageRoutingModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_1__["SharedModule"]
        ],
        declarations: [_detalle_lugar_page__WEBPACK_IMPORTED_MODULE_8__["DetalleLugarPage"], _reservaciones_nueva_reservacion_nueva_reservacion_component__WEBPACK_IMPORTED_MODULE_2__["NuevaReservacionComponent"]],
        entryComponents: [_reservaciones_nueva_reservacion_nueva_reservacion_component__WEBPACK_IMPORTED_MODULE_2__["NuevaReservacionComponent"]],
    })
], DetalleLugarPageModule);



/***/ }),

/***/ "./src/app/lugares/busqueda/detalle-lugar/detalle-lugar.page.scss":
/*!************************************************************************!*\
  !*** ./src/app/lugares/busqueda/detalle-lugar/detalle-lugar.page.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2x1Z2FyZXMvYnVzcXVlZGEvZGV0YWxsZS1sdWdhci9kZXRhbGxlLWx1Z2FyLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/lugares/busqueda/detalle-lugar/detalle-lugar.page.ts":
/*!**********************************************************************!*\
  !*** ./src/app/lugares/busqueda/detalle-lugar/detalle-lugar.page.ts ***!
  \**********************************************************************/
/*! exports provided: DetalleLugarPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalleLugarPage", function() { return DetalleLugarPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _shared_map_modal_map_modal_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../shared/map-modal/map-modal.component */ "./src/app/shared/map-modal/map-modal.component.ts");
/* harmony import */ var _lugares_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../lugares.service */ "./src/app/lugares/lugares.service.ts");
/* harmony import */ var _reservaciones_nueva_reservacion_nueva_reservacion_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../reservaciones/nueva-reservacion/nueva-reservacion.component */ "./src/app/reservaciones/nueva-reservacion/nueva-reservacion.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_reservaciones_reservacion_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/reservaciones/reservacion.service */ "./src/app/reservaciones/reservacion.service.ts");








let DetalleLugarPage = class DetalleLugarPage {
    constructor(router, navCtrl, modalCtrl, route, lugarService, actionSheetCtrl, reservacionService, loadingCtrl, alertCtrl) {
        this.router = router;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.route = route;
        this.lugarService = lugarService;
        this.actionSheetCtrl = actionSheetCtrl;
        this.reservacionService = reservacionService;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.isLoading = false;
    }
    ngOnInit() {
        this.route.paramMap.subscribe((paramMap) => {
            if (!paramMap.has("lugarId")) {
                this.navCtrl.navigateBack("/lugares/tab/busqueda");
                return;
            }
            this.isLoading = true;
            this.lugarSub = this.lugarService.getLugar(paramMap.get("lugarId")).subscribe(lugar => {
                this.lugarActual = lugar;
                this.isLoading = false;
            }, error => {
                this.alertCtrl.create({
                    header: 'Error',
                    message: 'Error al obtener el lugar !',
                    buttons: [
                        {
                            text: 'Ok', handler: () => {
                                this.router.navigate(['lugares/tabs/busqueda']);
                            }
                        }
                    ]
                }).then(alertEl => {
                    alertEl.present();
                });
            });
        });
    }
    ngOnDestroy() {
        if (this.lugarSub) {
            this.lugarSub.unsubscribe();
        }
    }
    onReservarLugar() {
        //this.navigateByUrl('lugares/tab/busqueda');
        //this.navCtrl.pop();
        //this.navCtrl.navigateBack("/lugares/tabs/busqueda");
        this.actionSheetCtrl.create({
            header: 'Selecciona accion',
            buttons: [
                { text: 'Seleccionar Fecha', handler: () => {
                        this.openReservarModal('select');
                    } },
                { text: 'Fecha al Azar', handler: () => {
                        this.openReservarModal('random');
                    } },
                { text: 'Cancelar', role: 'cancel' }
            ]
        })
            .then(actionSheetEl => {
            actionSheetEl.present();
        });
    }
    openReservarModal(mode) {
        console.log(mode);
        this.modalCtrl.create({ component: _reservaciones_nueva_reservacion_nueva_reservacion_component__WEBPACK_IMPORTED_MODULE_3__["NuevaReservacionComponent"],
            componentProps: { lugar: this.lugarActual, mode: mode } })
            .then((modalEl) => {
            modalEl.present();
            return modalEl.onDidDismiss();
        })
            .then((resultData) => {
            console.log(resultData);
            if (resultData.role === 'confirm') {
                this.loadingCtrl.create({ message: 'haciendo reservación ...' }).then(loadingEl => {
                    loadingEl.present();
                    const data = resultData.data.reservacion;
                    this.reservacionService.addReservacion(this.lugarActual.id, this.lugarActual.titulo, this.lugarActual.imageUrl, data.nombre, data.apellido, data.numeroHuespedes, data.desde, data.hasta).subscribe(() => {
                        loadingEl.dismiss();
                    });
                });
            }
        });
    }
    onMostrarMapa() {
        this.modalCtrl.create({ component: _shared_map_modal_map_modal_component__WEBPACK_IMPORTED_MODULE_1__["MapModalComponent"], componentProps: {
                center: { lat: this.lugarActual.ubicacion.lat, lng: this.lugarActual.ubicacion.lng },
                selectable: false,
                closeButtonText: 'Close',
                title: ''
            } }).then(modalEl => {
            modalEl.present();
        });
    }
};
DetalleLugarPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _lugares_service__WEBPACK_IMPORTED_MODULE_2__["LugaresService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ActionSheetController"] },
    { type: src_app_reservaciones_reservacion_service__WEBPACK_IMPORTED_MODULE_7__["ReservacionService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] }
];
DetalleLugarPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: "app-detalle-lugar",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./detalle-lugar.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/busqueda/detalle-lugar/detalle-lugar.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./detalle-lugar.page.scss */ "./src/app/lugares/busqueda/detalle-lugar/detalle-lugar.page.scss")).default]
    })
], DetalleLugarPage);



/***/ }),

/***/ "./src/app/reservaciones/nueva-reservacion/nueva-reservacion.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/reservaciones/nueva-reservacion/nueva-reservacion.component.scss ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Jlc2VydmFjaW9uZXMvbnVldmEtcmVzZXJ2YWNpb24vbnVldmEtcmVzZXJ2YWNpb24uY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/reservaciones/nueva-reservacion/nueva-reservacion.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/reservaciones/nueva-reservacion/nueva-reservacion.component.ts ***!
  \********************************************************************************/
/*! exports provided: NuevaReservacionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NuevaReservacionComponent", function() { return NuevaReservacionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let NuevaReservacionComponent = class NuevaReservacionComponent {
    constructor(modalCtrl) {
        this.modalCtrl = modalCtrl;
    }
    ngOnInit() {
        const disponibleDesde = new Date(this.lugar.disponibleDesde);
        const disponibleHasta = new Date(this.lugar.disponibleHasta);
        if (this.mode === 'random') {
            this.desde = new Date(disponibleDesde.getTime() + Math.random() * (disponibleHasta.getTime() - (7 * 24 * 60 * 60 * 1000) - disponibleDesde.getTime())).toISOString();
            this.hasta = new Date(new Date(new Date(this.desde).getTime() + Math.random() * (new Date(this.desde).getTime() + (6 * 24 * 60 * 60 * 1000) - new Date(this.desde).getTime()))).toISOString();
        }
    }
    onBookPlace() {
        if (!this.myform.valid || !this.fechasValidas()) {
            return;
        }
        this.modalCtrl.dismiss({ reservacion: {
                nombre: this.myform.value['nombre'],
                apellido: this.myform.value['apellido'],
                numeroHuespedes: +this.myform.value['numero.huespedes'],
                desde: new Date(this.myform.value['fec-desde']),
                hasta: new Date(this.myform.value['fec-hasta'])
            } }, 'confirm');
    }
    onCancel() {
        this.modalCtrl.dismiss(null, 'cancel');
    }
    fechasValidas() {
        try {
            const inicio = new Date(this.myform.value['fec-desde']);
            const fin = new Date(this.myform.value['fec-hasta']);
            return fin > inicio;
        }
        catch (ex) {
            return false;
        }
    }
};
NuevaReservacionComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
];
NuevaReservacionComponent.propDecorators = {
    lugar: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }],
    mode: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"] }],
    myform: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ['formNew',] }]
};
NuevaReservacionComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-nueva-reservacion',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./nueva-reservacion.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/reservaciones/nueva-reservacion/nueva-reservacion.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./nueva-reservacion.component.scss */ "./src/app/reservaciones/nueva-reservacion/nueva-reservacion.component.scss")).default]
    })
], NuevaReservacionComponent);



/***/ }),

/***/ "./src/app/reservaciones/reservacion.model.ts":
/*!****************************************************!*\
  !*** ./src/app/reservaciones/reservacion.model.ts ***!
  \****************************************************/
/*! exports provided: Reservacion */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Reservacion", function() { return Reservacion; });
class Reservacion {
    constructor(id, lugarId, usuarioId, lugarTitulo, imageUrl, nombre, apellido, huespedes, desde, hasta, firebaseId) {
        this.id = id;
        this.lugarId = lugarId;
        this.usuarioId = usuarioId;
        this.lugarTitulo = lugarTitulo;
        this.imageUrl = imageUrl;
        this.nombre = nombre;
        this.apellido = apellido;
        this.huespedes = huespedes;
        this.desde = desde;
        this.hasta = hasta;
        this.firebaseId = firebaseId;
    }
}


/***/ }),

/***/ "./src/app/reservaciones/reservacion.service.ts":
/*!******************************************************!*\
  !*** ./src/app/reservaciones/reservacion.service.ts ***!
  \******************************************************/
/*! exports provided: ReservacionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReservacionService", function() { return ReservacionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _login_login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../login/login.service */ "./src/app/login/login.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _reservacion_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./reservacion.model */ "./src/app/reservaciones/reservacion.model.ts");







let ReservacionService = class ReservacionService {
    constructor(loginService, http) {
        this.loginService = loginService;
        this.http = http;
        this._reservaciones = new rxjs__WEBPACK_IMPORTED_MODULE_5__["BehaviorSubject"]([]);
    }
    get reservaciones() {
        return this._reservaciones.asObservable();
    }
    fetchReservaciones() {
        let url = 'https://practica10-5b315.firebaseio.com/reservaciones.json?orderBy="usuarioId"&equalTo=${this.loginService.usuarioId}';
        return this.http.get(url).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(rsvDta => {
            const reservaciones = [];
            for (const key in rsvDta) {
                if (rsvDta.hasOwnProperty(key)) {
                    reservaciones.push(new _reservacion_model__WEBPACK_IMPORTED_MODULE_6__["Reservacion"](rsvDta[key].id, rsvDta[key].lugarId, rsvDta[key].usuarioId, rsvDta[key].lugarTitulo, rsvDta[key].imageUrl, rsvDta[key].nombre, rsvDta[key].apellido, rsvDta[key].huespedes, new Date(rsvDta[key].desde), new Date(rsvDta[key].hasta), key));
                }
            }
            return reservaciones;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["tap"])(rsvs => {
            this._reservaciones.next(rsvs);
        }));
    }
    addReservacion(lugarId, descripcion, imageUrl, nombre, apellido, huespedes, desde, hasta) {
        let firebaseId;
        const newReservacion = new _reservacion_model__WEBPACK_IMPORTED_MODULE_6__["Reservacion"](Math.random() * 100, lugarId, this.loginService.usuarioId, descripcion, imageUrl, nombre, apellido, huespedes, desde, hasta, null);
        return this.http.post('https://proyecto10-390e5.firebaseio.com/reservaciones.json', Object.assign(Object.assign({}, newReservacion), { firebaseId: null })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["switchMap"])(resData => {
            firebaseId = resData.name;
            return this.reservaciones;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["tap"])(rsvs => {
            this._reservaciones.next(rsvs.concat(newReservacion));
        }));
        /*
        console.log(newReservacion);
        return this.reservaciones.pipe(take(1 ), delay(1000),
        tap(rsvs => {
        this._reservaciones.next(rsvs.concat(newReservacion));
        }));
        */
    }
    cancelarReservacion(firebaseId) {
        return this.http.delete('https://proyecto10-390e5.firebaseio.com//reservaciones/${firebaseId}.json')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["switchMap"])(() => {
            return this.reservaciones;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["tap"])(rsvs => {
            this._reservaciones.next(rsvs.filter(r => {
                r.firebaseId !== firebaseId;
            }));
        }));
    }
};
ReservacionService.ctorParameters = () => [
    { type: _login_login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ReservacionService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Injectable"])({
        providedIn: 'root'
    })
], ReservacionService);



/***/ })

}]);
//# sourceMappingURL=default~busqueda-detalle-lugar-detalle-lugar-module~detalle-lugar-detalle-lugar-module-es2015.js.map