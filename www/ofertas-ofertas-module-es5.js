(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ofertas-ofertas-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/ofertas.page.html":
    /*!*****************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/ofertas.page.html ***!
      \*****************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppLugaresOfertasOfertasPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button menu=\"menu1\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Offers</ion-title>\n    <ion-buttons slot=\"primary\">\n      <ion-button routerLink=\"/lugares/tabs/ofertas/new\">\n        <ion-icon name=\"add\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid fixed>\n    <ion-row>\n      <ion-col size=\"12\" size-sm=\"8\" offset-sm=\"2\" text-center>\n          <div class=\"ion-text-center\" *ngIf=\"isLoading\">\n              <ion-spinner color=\"primary\"></ion-spinner>\n          </div>\n          <ion-list *ngIf=\"!isLoading\"></ion-list>\n          <ion-item-sliding *ngFor=\"let lugar of ofertas\">\n            <ion-item\n              [routerLink]=\"['/','lugares', 'tabs', 'ofertas', lugar.firebaseId]\"\n            >\n              <ion-thumbnail slot=\"start\">\n                <ion-img [src]=\"lugar.imageUrl\"></ion-img>\n              </ion-thumbnail>\n              <ion-label>\n                <h2>{{lugar.titulo}}</h2>\n              </ion-label>\n            </ion-item>\n            <ion-item-options>\n              <ion-item-option color=\"secondary\" (click)=\"onEdit(lugar.firebaseId)\">\n                <ion-icon name=\"brush-outline\" slot=\"icon-only\"></ion-icon>\n              </ion-item-option>\n            </ion-item-options>\n          </ion-item-sliding>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/lugares/ofertas/ofertas-routing.module.ts":
    /*!***********************************************************!*\
      !*** ./src/app/lugares/ofertas/ofertas-routing.module.ts ***!
      \***********************************************************/

    /*! exports provided: OfertasPageRoutingModule */

    /***/
    function srcAppLugaresOfertasOfertasRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OfertasPageRoutingModule", function () {
        return OfertasPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ofertas_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./ofertas.page */
      "./src/app/lugares/ofertas/ofertas.page.ts");

      var routes = [{
        path: "",
        component: _ofertas_page__WEBPACK_IMPORTED_MODULE_3__["OfertasPage"]
      }, {
        path: "new",
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | nueva-oferta-nueva-oferta-module */
          [__webpack_require__.e("default~busqueda-detalle-lugar-detalle-lugar-module~detalle-lugar-detalle-lugar-module~nueva-oferta-~5277d145"), __webpack_require__.e("common")]).then(__webpack_require__.bind(null,
          /*! ./nueva-oferta/nueva-oferta.module */
          "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.module.ts")).then(function (m) {
            return m.NuevaOfertaPageModule;
          });
        }
      }, {
        path: "edit",
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | editar-oferta-editar-oferta-module */
          "common").then(__webpack_require__.bind(null,
          /*! ./editar-oferta/editar-oferta.module */
          "./src/app/lugares/ofertas/editar-oferta/editar-oferta.module.ts")).then(function (m) {
            return m.EditarOfertaPageModule;
          });
        }
      }, {
        path: "reservar",
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | reservar-oferta-reservar-oferta-module */
          "common").then(__webpack_require__.bind(null,
          /*! ./reservar-oferta/reservar-oferta.module */
          "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.module.ts")).then(function (m) {
            return m.ReservarOfertaPageModule;
          });
        }
      }];

      var OfertasPageRoutingModule = function OfertasPageRoutingModule() {
        _classCallCheck(this, OfertasPageRoutingModule);
      };

      OfertasPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], OfertasPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/lugares/ofertas/ofertas.module.ts":
    /*!***************************************************!*\
      !*** ./src/app/lugares/ofertas/ofertas.module.ts ***!
      \***************************************************/

    /*! exports provided: OfertasPageModule */

    /***/
    function srcAppLugaresOfertasOfertasModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OfertasPageModule", function () {
        return OfertasPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ofertas_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./ofertas-routing.module */
      "./src/app/lugares/ofertas/ofertas-routing.module.ts");
      /* harmony import */


      var _ofertas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./ofertas.page */
      "./src/app/lugares/ofertas/ofertas.page.ts");

      var OfertasPageModule = function OfertasPageModule() {
        _classCallCheck(this, OfertasPageModule);
      };

      OfertasPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _ofertas_routing_module__WEBPACK_IMPORTED_MODULE_5__["OfertasPageRoutingModule"]],
        declarations: [_ofertas_page__WEBPACK_IMPORTED_MODULE_6__["OfertasPage"]]
      })], OfertasPageModule);
      /***/
    },

    /***/
    "./src/app/lugares/ofertas/ofertas.page.scss":
    /*!***************************************************!*\
      !*** ./src/app/lugares/ofertas/ofertas.page.scss ***!
      \***************************************************/

    /*! exports provided: default */

    /***/
    function srcAppLugaresOfertasOfertasPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2x1Z2FyZXMvb2ZlcnRhcy9vZmVydGFzLnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/lugares/ofertas/ofertas.page.ts":
    /*!*************************************************!*\
      !*** ./src/app/lugares/ofertas/ofertas.page.ts ***!
      \*************************************************/

    /*! exports provided: OfertasPage */

    /***/
    function srcAppLugaresOfertasOfertasPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OfertasPage", function () {
        return OfertasPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _lugares_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../lugares.service */
      "./src/app/lugares/lugares.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var OfertasPage = /*#__PURE__*/function () {
        function OfertasPage(offersService, router) {
          _classCallCheck(this, OfertasPage);

          this.offersService = offersService;
          this.router = router;
          this.isLoading = false;
        }

        _createClass(OfertasPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.lugaresSub = this.offersService.lugares.subscribe(function (lugares) {
              _this.ofertas = lugares;
            });
          }
        }, {
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            var _this2 = this;

            this.isLoading = true;
            this.offersService.fetchLugares().subscribe(function () {
              _this2.isLoading = false;
            });
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            if (this.lugaresSub) {
              this.lugaresSub.unsubscribe();
            }
          }
        }, {
          key: "onEdit",
          value: function onEdit(firebaseId) {
            this.router.navigate(['/', 'lugares', 'tabs', 'ofertas', 'edit', firebaseId]);
          }
        }]);

        return OfertasPage;
      }();

      OfertasPage.ctorParameters = function () {
        return [{
          type: _lugares_service__WEBPACK_IMPORTED_MODULE_1__["LugaresService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }];
      };

      OfertasPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: "app-ofertas",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./ofertas.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/ofertas.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./ofertas.page.scss */
        "./src/app/lugares/ofertas/ofertas.page.scss"))["default"]]
      })], OfertasPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=ofertas-ofertas-module-es5.js.map