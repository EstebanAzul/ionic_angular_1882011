(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["lugares-lugares-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/lugares.page.html":
    /*!*********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/lugares.page.html ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppLugaresLugaresPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-tabs>\n  <ion-tab-bar slot=\"bottom\">\n    <ion-tab-button tab=\"busqueda\">\n      <ion-icon name=\"search\"></ion-icon>\n      <ion-label>Discover</ion-label>\n    </ion-tab-button>\n    <ion-tab-button tab=\"ofertas\">\n      <ion-icon name=\"card\"></ion-icon>\n      <ion-label>Offers</ion-label>\n    </ion-tab-button>\n  </ion-tab-bar>\n</ion-tabs>\n";
      /***/
    },

    /***/
    "./src/app/lugares/lugares-routing.module.ts":
    /*!***************************************************!*\
      !*** ./src/app/lugares/lugares-routing.module.ts ***!
      \***************************************************/

    /*! exports provided: LugaresPageRoutingModule */

    /***/
    function srcAppLugaresLugaresRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LugaresPageRoutingModule", function () {
        return LugaresPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _lugares_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./lugares.page */
      "./src/app/lugares/lugares.page.ts");

      var routes01 = [{
        path: "",
        component: _lugares_page__WEBPACK_IMPORTED_MODULE_3__["LugaresPage"]
      }];
      var routes = [{
        path: "tabs",
        component: _lugares_page__WEBPACK_IMPORTED_MODULE_3__["LugaresPage"],
        children: [{
          path: "busqueda",
          children: [{
            path: "",
            loadChildren: function loadChildren() {
              return Promise.all(
              /*! import() | busqueda-busqueda-module */
              [__webpack_require__.e("common"), __webpack_require__.e("busqueda-busqueda-module")]).then(__webpack_require__.bind(null,
              /*! ./busqueda/busqueda.module */
              "./src/app/lugares/busqueda/busqueda.module.ts")).then(function (m) {
                return m.BusquedaPageModule;
              });
            }
          }, {
            path: ":lugarId",
            loadChildren: function loadChildren() {
              return Promise.all(
              /*! import() | busqueda-detalle-lugar-detalle-lugar-module */
              [__webpack_require__.e("default~busqueda-detalle-lugar-detalle-lugar-module~detalle-lugar-detalle-lugar-module~nueva-oferta-~5277d145"), __webpack_require__.e("default~busqueda-detalle-lugar-detalle-lugar-module~detalle-lugar-detalle-lugar-module"), __webpack_require__.e("common")]).then(__webpack_require__.bind(null,
              /*! ./busqueda/detalle-lugar/detalle-lugar.module */
              "./src/app/lugares/busqueda/detalle-lugar/detalle-lugar.module.ts")).then(function (m) {
                return m.DetalleLugarPageModule;
              });
            }
          }]
        }, {
          path: "ofertas",
          children: [{
            path: "",
            loadChildren: function loadChildren() {
              return Promise.all(
              /*! import() | ofertas-ofertas-module */
              [__webpack_require__.e("common"), __webpack_require__.e("ofertas-ofertas-module")]).then(__webpack_require__.bind(null,
              /*! ./ofertas/ofertas.module */
              "./src/app/lugares/ofertas/ofertas.module.ts")).then(function (m) {
                return m.OfertasPageModule;
              });
            }
          }, {
            path: "new",
            loadChildren: function loadChildren() {
              return Promise.all(
              /*! import() | ofertas-nueva-oferta-nueva-oferta-module */
              [__webpack_require__.e("default~busqueda-detalle-lugar-detalle-lugar-module~detalle-lugar-detalle-lugar-module~nueva-oferta-~5277d145"), __webpack_require__.e("common")]).then(__webpack_require__.bind(null,
              /*! ./ofertas/nueva-oferta/nueva-oferta.module */
              "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.module.ts")).then(function (m) {
                return m.NuevaOfertaPageModule;
              });
            }
          }, {
            path: "edit/:lugarId",
            loadChildren: function loadChildren() {
              return __webpack_require__.e(
              /*! import() | ofertas-editar-oferta-editar-oferta-module */
              "common").then(__webpack_require__.bind(null,
              /*! ./ofertas/editar-oferta/editar-oferta.module */
              "./src/app/lugares/ofertas/editar-oferta/editar-oferta.module.ts")).then(function (m) {
                return m.EditarOfertaPageModule;
              });
            }
          }, {
            path: ":lugarId",
            loadChildren: function loadChildren() {
              return __webpack_require__.e(
              /*! import() | ofertas-reservar-oferta-reservar-oferta-module */
              "common").then(__webpack_require__.bind(null,
              /*! ./ofertas/reservar-oferta/reservar-oferta.module */
              "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.module.ts")).then(function (m) {
                return m.ReservarOfertaPageModule;
              });
            }
          }]
        }]
      }, {
        path: "",
        redirectTo: "/lugares/tabs/busqueda",
        pathMatch: "full"
      }]; // {
      //   path: "busqueda",
      //   loadChildren: () =>
      //     import("./busqueda/busqueda.module").then((m) => m.BusquedaPageModule),
      // },
      // {
      //   path: "ofertas",
      //   loadChildren: () =>
      //     import("./ofertas/ofertas.module").then((m) => m.OfertasPageModule),
      // },
      // {
      //   path: "lugares",
      //   loadChildren: () =>
      //     import("./lugares/lugares.module").then((m) => m.LugaresPageModule),
      // },

      var LugaresPageRoutingModule = function LugaresPageRoutingModule() {
        _classCallCheck(this, LugaresPageRoutingModule);
      };

      LugaresPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LugaresPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/lugares/lugares.module.ts":
    /*!*******************************************!*\
      !*** ./src/app/lugares/lugares.module.ts ***!
      \*******************************************/

    /*! exports provided: LugaresPageModule */

    /***/
    function srcAppLugaresLugaresModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LugaresPageModule", function () {
        return LugaresPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _lugares_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./lugares-routing.module */
      "./src/app/lugares/lugares-routing.module.ts");
      /* harmony import */


      var _lugares_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./lugares.page */
      "./src/app/lugares/lugares.page.ts");

      var LugaresPageModule = function LugaresPageModule() {
        _classCallCheck(this, LugaresPageModule);
      };

      LugaresPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _lugares_routing_module__WEBPACK_IMPORTED_MODULE_5__["LugaresPageRoutingModule"]],
        declarations: [_lugares_page__WEBPACK_IMPORTED_MODULE_6__["LugaresPage"]]
      })], LugaresPageModule);
      /***/
    },

    /***/
    "./src/app/lugares/lugares.page.scss":
    /*!*******************************************!*\
      !*** ./src/app/lugares/lugares.page.scss ***!
      \*******************************************/

    /*! exports provided: default */

    /***/
    function srcAppLugaresLugaresPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2x1Z2FyZXMvbHVnYXJlcy5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/lugares/lugares.page.ts":
    /*!*****************************************!*\
      !*** ./src/app/lugares/lugares.page.ts ***!
      \*****************************************/

    /*! exports provided: LugaresPage */

    /***/
    function srcAppLugaresLugaresPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LugaresPage", function () {
        return LugaresPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var LugaresPage = /*#__PURE__*/function () {
        function LugaresPage() {
          _classCallCheck(this, LugaresPage);
        }

        _createClass(LugaresPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return LugaresPage;
      }();

      LugaresPage.ctorParameters = function () {
        return [];
      };

      LugaresPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-lugares',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./lugares.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/lugares.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./lugares.page.scss */
        "./src/app/lugares/lugares.page.scss"))["default"]]
      })], LugaresPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=lugares-lugares-module-es5.js.map