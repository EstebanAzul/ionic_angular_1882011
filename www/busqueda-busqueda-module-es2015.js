(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["busqueda-busqueda-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/busqueda/busqueda.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/busqueda/busqueda.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button menu=\"menu1\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Discover</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-segment (ionChange)=\"onFilterUpdate($event)\" value=\"todos\">\n    <ion-segment-button value=\"todos\">\n      All Places\n    </ion-segment-button>\n    <ion-segment-button value=\"disponibles\">\n      <ion-label>Available</ion-label>\n    </ion-segment-button>\n  </ion-segment>\n<ion-grid *ngIf= \"isLoading\">\n  <ion-row>\n    <ion-col size=\"12\" size-sm=\"8\" offset-sm=\"2\">\n      <ion-spinner color=\"primary\"></ion-spinner>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n<ion-grid *ngIf=\"!isLoading && (!lugaresRelevantes || lugaresRelevantes.length <= 0)\">\n<ion-row>\n<ion-col size=\" 12\" size-sm=\"8\" offset-sm=\"2\">\n<p>No hay lugares disponibles en este momento, regrese luego</p>\n</ion-col>\n</ion-row>\n</ion-grid>\n<ion-grid *ngIf=\"!isLoading && lugaresRelevantes.length > 0\">\n    <ion-row>\n      <ion-col size=\"12\" size-sm=\"8\" offset-sm=\"2\" text-center>\n        <ion-card>\n          <ion-card-header>\n            <ion-card-title>{{lugaresRelevantes[0].titulo}}</ion-card-title>\n            <ion-card-subtitle\n              >{{lugaresRelevantes[0].precio | currency}} / por\n              noche</ion-card-subtitle\n            >\n          </ion-card-header>\n          <ion-img [src]=\"lugaresRelevantes[0].imageUrl\"></ion-img>\n          <ion-card-content>\n            <p>{{lugaresRelevantes[0].descripcion}}</p>\n          </ion-card-content>\n          <div class=\"ion-text-right\">\n            <ion-button\n              fill=\"clear\"\n              color=\"primary\"\n              [routerLink]=\"['/', 'lugares', 'tabs', 'busqueda', lugaresRelevantes[0].firebaseId]\">Mas</ion-button>\n          </div>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\" size-sm=\"8\" offset-sm=\"2\" text-center>\n          <ion-virtual-scroll [items]=\"lugaresListados\" apprxItemHeight=\"72px\">\n            <ion-item [routerLink]=\"['/', 'lugares','tabs', 'busqueda', lugar.farebaseId]\" detail  *virtualItem=\"let lugar\" >\n            <ion-thumbnail slot=\"start\">\n              <ion-img [src]=\"lugar.imageUrl\"></ion-img>\n            </ion-thumbnail>\n            <ion-label>\n              <h2>{{lugar.titulo}}</h2>\n            </ion-label>\n          </ion-item>\n          </ion-virtual-scroll>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>");

/***/ }),

/***/ "./src/app/lugares/busqueda/busqueda-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/lugares/busqueda/busqueda-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: BusquedaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BusquedaPageRoutingModule", function() { return BusquedaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _busqueda_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./busqueda.page */ "./src/app/lugares/busqueda/busqueda.page.ts");




const routes = [
    {
        path: '',
        component: _busqueda_page__WEBPACK_IMPORTED_MODULE_3__["BusquedaPage"]
    },
    {
        path: 'detalle-lugar',
        loadChildren: () => Promise.all(/*! import() | detalle-lugar-detalle-lugar-module */[__webpack_require__.e("default~busqueda-detalle-lugar-detalle-lugar-module~detalle-lugar-detalle-lugar-module~nueva-oferta-~5277d145"), __webpack_require__.e("default~busqueda-detalle-lugar-detalle-lugar-module~detalle-lugar-detalle-lugar-module")]).then(__webpack_require__.bind(null, /*! ./detalle-lugar/detalle-lugar.module */ "./src/app/lugares/busqueda/detalle-lugar/detalle-lugar.module.ts")).then(m => m.DetalleLugarPageModule)
    }
];
let BusquedaPageRoutingModule = class BusquedaPageRoutingModule {
};
BusquedaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], BusquedaPageRoutingModule);



/***/ }),

/***/ "./src/app/lugares/busqueda/busqueda.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/lugares/busqueda/busqueda.module.ts ***!
  \*****************************************************/
/*! exports provided: BusquedaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BusquedaPageModule", function() { return BusquedaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _busqueda_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./busqueda-routing.module */ "./src/app/lugares/busqueda/busqueda-routing.module.ts");
/* harmony import */ var _busqueda_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./busqueda.page */ "./src/app/lugares/busqueda/busqueda.page.ts");







let BusquedaPageModule = class BusquedaPageModule {
};
BusquedaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _busqueda_routing_module__WEBPACK_IMPORTED_MODULE_5__["BusquedaPageRoutingModule"]
        ],
        declarations: [_busqueda_page__WEBPACK_IMPORTED_MODULE_6__["BusquedaPage"]]
    })
], BusquedaPageModule);



/***/ }),

/***/ "./src/app/lugares/busqueda/busqueda.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/lugares/busqueda/busqueda.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2x1Z2FyZXMvYnVzcXVlZGEvYnVzcXVlZGEucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/lugares/busqueda/busqueda.page.ts":
/*!***************************************************!*\
  !*** ./src/app/lugares/busqueda/busqueda.page.ts ***!
  \***************************************************/
/*! exports provided: BusquedaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BusquedaPage", function() { return BusquedaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _login_login_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../login/login.service */ "./src/app/login/login.service.ts");
/* harmony import */ var _lugares_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../lugares.service */ "./src/app/lugares/lugares.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");





let BusquedaPage = class BusquedaPage {
    constructor(lugaresService, menuCtrl, loginService) {
        this.lugaresService = lugaresService;
        this.menuCtrl = menuCtrl;
        this.loginService = loginService;
        this.isLoading = false;
    }
    ngOnInit() {
        this.lugaresSub = this.lugaresService.lugares.subscribe(lugares => {
            this.lugaresCargados = lugares;
            this.lugaresRelevantes = this.lugaresCargados;
            this.lugaresListados = this.lugaresRelevantes.slice(1);
        });
    }
    ionViewWillEnter() {
        this.isLoading = true;
        this.lugaresService.fetchLugares().subscribe(() => {
            this.isLoading = false;
        });
    }
    ngOnDestroy() {
        if (this.lugaresSub) {
            this.lugaresSub.unsubscribe();
        }
    }
    OpenSideMenu() {
        console.log("Side menu");
        this.menuCtrl.open();
    }
    onFilterUpdate(event) {
        console.log(event.detail);
        if (event.detail.value === 'todos') {
            this.lugaresRelevantes = this.lugaresCargados;
            this.lugaresListados = this.lugaresRelevantes.slice(1);
        }
        else {
            this.lugaresRelevantes = this.lugaresCargados.filter(lugar => lugar.usuarioId !== this.loginService.usuarioId);
            this.lugaresListados = this.lugaresRelevantes.slice(1);
        }
    }
};
BusquedaPage.ctorParameters = () => [
    { type: _lugares_service__WEBPACK_IMPORTED_MODULE_2__["LugaresService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
    { type: _login_login_service__WEBPACK_IMPORTED_MODULE_1__["LoginService"] }
];
BusquedaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: "app-busqueda",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./busqueda.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/busqueda/busqueda.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./busqueda.page.scss */ "./src/app/lugares/busqueda/busqueda.page.scss")).default]
    })
], BusquedaPage);



/***/ })

}]);
//# sourceMappingURL=busqueda-busqueda-module-es2015.js.map