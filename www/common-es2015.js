(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./node_modules/@ionic/core/dist/esm/button-active-5da929d4.js":
/*!*********************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/button-active-5da929d4.js ***!
  \*********************************************************************/
/*! exports provided: c */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return createButtonActiveGesture; });
/* harmony import */ var _index_92848855_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index-92848855.js */ "./node_modules/@ionic/core/dist/esm/index-92848855.js");
/* harmony import */ var _index_eea61379_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index-eea61379.js */ "./node_modules/@ionic/core/dist/esm/index-eea61379.js");
/* harmony import */ var _haptic_7b8ba70a_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./haptic-7b8ba70a.js */ "./node_modules/@ionic/core/dist/esm/haptic-7b8ba70a.js");




const createButtonActiveGesture = (el, isButton) => {
    let currentTouchedButton;
    let initialTouchedButton;
    const activateButtonAtPoint = (x, y, hapticFeedbackFn) => {
        if (typeof document === 'undefined') {
            return;
        }
        const target = document.elementFromPoint(x, y);
        if (!target || !isButton(target)) {
            clearActiveButton();
            return;
        }
        if (target !== currentTouchedButton) {
            clearActiveButton();
            setActiveButton(target, hapticFeedbackFn);
        }
    };
    const setActiveButton = (button, hapticFeedbackFn) => {
        currentTouchedButton = button;
        if (!initialTouchedButton) {
            initialTouchedButton = currentTouchedButton;
        }
        const buttonToModify = currentTouchedButton;
        Object(_index_92848855_js__WEBPACK_IMPORTED_MODULE_0__["c"])(() => buttonToModify.classList.add('ion-activated'));
        hapticFeedbackFn();
    };
    const clearActiveButton = (dispatchClick = false) => {
        if (!currentTouchedButton) {
            return;
        }
        const buttonToModify = currentTouchedButton;
        Object(_index_92848855_js__WEBPACK_IMPORTED_MODULE_0__["c"])(() => buttonToModify.classList.remove('ion-activated'));
        /**
         * Clicking on one button, but releasing on another button
         * does not dispatch a click event in browsers, so we
         * need to do it manually here. Some browsers will
         * dispatch a click if clicking on one button, dragging over
         * another button, and releasing on the original button. In that
         * case, we need to make sure we do not cause a double click there.
         */
        if (dispatchClick && initialTouchedButton !== currentTouchedButton) {
            currentTouchedButton.click();
        }
        currentTouchedButton = undefined;
    };
    return Object(_index_eea61379_js__WEBPACK_IMPORTED_MODULE_1__["createGesture"])({
        el,
        gestureName: 'buttonActiveDrag',
        threshold: 0,
        onStart: ev => activateButtonAtPoint(ev.currentX, ev.currentY, _haptic_7b8ba70a_js__WEBPACK_IMPORTED_MODULE_2__["a"]),
        onMove: ev => activateButtonAtPoint(ev.currentX, ev.currentY, _haptic_7b8ba70a_js__WEBPACK_IMPORTED_MODULE_2__["b"]),
        onEnd: () => {
            clearActiveButton(true);
            Object(_haptic_7b8ba70a_js__WEBPACK_IMPORTED_MODULE_2__["h"])();
            initialTouchedButton = undefined;
        }
    });
};




/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm/framework-delegate-d1eb6504.js":
/*!**************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/framework-delegate-d1eb6504.js ***!
  \**************************************************************************/
/*! exports provided: a, d */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return attachComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return detachComponent; });
const attachComponent = async (delegate, container, component, cssClasses, componentProps) => {
    if (delegate) {
        return delegate.attachViewToDom(container, component, componentProps, cssClasses);
    }
    if (typeof component !== 'string' && !(component instanceof HTMLElement)) {
        throw new Error('framework delegate is missing');
    }
    const el = (typeof component === 'string')
        ? container.ownerDocument && container.ownerDocument.createElement(component)
        : component;
    if (cssClasses) {
        cssClasses.forEach(c => el.classList.add(c));
    }
    if (componentProps) {
        Object.assign(el, componentProps);
    }
    container.appendChild(el);
    if (el.componentOnReady) {
        await el.componentOnReady();
    }
    return el;
};
const detachComponent = (delegate, element) => {
    if (element) {
        if (delegate) {
            const container = element.parentElement;
            return delegate.removeViewFromDom(container, element);
        }
        element.remove();
    }
    return Promise.resolve();
};




/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm/haptic-7b8ba70a.js":
/*!**************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/haptic-7b8ba70a.js ***!
  \**************************************************************/
/*! exports provided: a, b, c, d, h */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return hapticSelectionStart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return hapticSelectionChanged; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return hapticSelection; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return hapticImpact; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return hapticSelectionEnd; });
const HapticEngine = {
    getEngine() {
        const win = window;
        return (win.TapticEngine) || (win.Capacitor && win.Capacitor.isPluginAvailable('Haptics') && win.Capacitor.Plugins.Haptics);
    },
    available() {
        return !!this.getEngine();
    },
    isCordova() {
        return !!window.TapticEngine;
    },
    isCapacitor() {
        const win = window;
        return !!win.Capacitor;
    },
    impact(options) {
        const engine = this.getEngine();
        if (!engine) {
            return;
        }
        const style = this.isCapacitor() ? options.style.toUpperCase() : options.style;
        engine.impact({ style });
    },
    notification(options) {
        const engine = this.getEngine();
        if (!engine) {
            return;
        }
        const style = this.isCapacitor() ? options.style.toUpperCase() : options.style;
        engine.notification({ style });
    },
    selection() {
        this.impact({ style: 'light' });
    },
    selectionStart() {
        const engine = this.getEngine();
        if (!engine) {
            return;
        }
        if (this.isCapacitor()) {
            engine.selectionStart();
        }
        else {
            engine.gestureSelectionStart();
        }
    },
    selectionChanged() {
        const engine = this.getEngine();
        if (!engine) {
            return;
        }
        if (this.isCapacitor()) {
            engine.selectionChanged();
        }
        else {
            engine.gestureSelectionChanged();
        }
    },
    selectionEnd() {
        const engine = this.getEngine();
        if (!engine) {
            return;
        }
        if (this.isCapacitor()) {
            engine.selectionEnd();
        }
        else {
            engine.gestureSelectionEnd();
        }
    }
};
/**
 * Trigger a selection changed haptic event. Good for one-time events
 * (not for gestures)
 */
const hapticSelection = () => {
    HapticEngine.selection();
};
/**
 * Tell the haptic engine that a gesture for a selection change is starting.
 */
const hapticSelectionStart = () => {
    HapticEngine.selectionStart();
};
/**
 * Tell the haptic engine that a selection changed during a gesture.
 */
const hapticSelectionChanged = () => {
    HapticEngine.selectionChanged();
};
/**
 * Tell the haptic engine we are done with a gesture. This needs to be
 * called lest resources are not properly recycled.
 */
const hapticSelectionEnd = () => {
    HapticEngine.selectionEnd();
};
/**
 * Use this to indicate success/failure/warning to the user.
 * options should be of the type `{ style: 'light' }` (or `medium`/`heavy`)
 */
const hapticImpact = (options) => {
    HapticEngine.impact(options);
};




/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm/spinner-configs-c78e170e.js":
/*!***********************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/spinner-configs-c78e170e.js ***!
  \***********************************************************************/
/*! exports provided: S */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "S", function() { return SPINNERS; });
const spinners = {
    'bubbles': {
        dur: 1000,
        circles: 9,
        fn: (dur, index, total) => {
            const animationDelay = `${(dur * index / total) - dur}ms`;
            const angle = 2 * Math.PI * index / total;
            return {
                r: 5,
                style: {
                    'top': `${9 * Math.sin(angle)}px`,
                    'left': `${9 * Math.cos(angle)}px`,
                    'animation-delay': animationDelay,
                }
            };
        }
    },
    'circles': {
        dur: 1000,
        circles: 8,
        fn: (dur, index, total) => {
            const step = index / total;
            const animationDelay = `${(dur * step) - dur}ms`;
            const angle = 2 * Math.PI * step;
            return {
                r: 5,
                style: {
                    'top': `${9 * Math.sin(angle)}px`,
                    'left': `${9 * Math.cos(angle)}px`,
                    'animation-delay': animationDelay,
                }
            };
        }
    },
    'circular': {
        dur: 1400,
        elmDuration: true,
        circles: 1,
        fn: () => {
            return {
                r: 20,
                cx: 48,
                cy: 48,
                fill: 'none',
                viewBox: '24 24 48 48',
                transform: 'translate(0,0)',
                style: {}
            };
        }
    },
    'crescent': {
        dur: 750,
        circles: 1,
        fn: () => {
            return {
                r: 26,
                style: {}
            };
        }
    },
    'dots': {
        dur: 750,
        circles: 3,
        fn: (_, index) => {
            const animationDelay = -(110 * index) + 'ms';
            return {
                r: 6,
                style: {
                    'left': `${9 - (9 * index)}px`,
                    'animation-delay': animationDelay,
                }
            };
        }
    },
    'lines': {
        dur: 1000,
        lines: 12,
        fn: (dur, index, total) => {
            const transform = `rotate(${30 * index + (index < 6 ? 180 : -180)}deg)`;
            const animationDelay = `${(dur * index / total) - dur}ms`;
            return {
                y1: 17,
                y2: 29,
                style: {
                    'transform': transform,
                    'animation-delay': animationDelay,
                }
            };
        }
    },
    'lines-small': {
        dur: 1000,
        lines: 12,
        fn: (dur, index, total) => {
            const transform = `rotate(${30 * index + (index < 6 ? 180 : -180)}deg)`;
            const animationDelay = `${(dur * index / total) - dur}ms`;
            return {
                y1: 12,
                y2: 20,
                style: {
                    'transform': transform,
                    'animation-delay': animationDelay,
                }
            };
        }
    }
};
const SPINNERS = spinners;




/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm/theme-5641d27f.js":
/*!*************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/theme-5641d27f.js ***!
  \*************************************************************/
/*! exports provided: c, g, h, o */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return createColorClasses; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return getClassMap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return hostContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "o", function() { return openURL; });
const hostContext = (selector, el) => {
    return el.closest(selector) !== null;
};
/**
 * Create the mode and color classes for the component based on the classes passed in
 */
const createColorClasses = (color, cssClassMap) => {
    return (typeof color === 'string' && color.length > 0) ? Object.assign({ 'ion-color': true, [`ion-color-${color}`]: true }, cssClassMap) : cssClassMap;
};
const getClassList = (classes) => {
    if (classes !== undefined) {
        const array = Array.isArray(classes) ? classes : classes.split(' ');
        return array
            .filter(c => c != null)
            .map(c => c.trim())
            .filter(c => c !== '');
    }
    return [];
};
const getClassMap = (classes) => {
    const map = {};
    getClassList(classes).forEach(c => map[c] = true);
    return map;
};
const SCHEME = /^[a-z][a-z0-9+\-.]*:/;
const openURL = async (url, ev, direction, animation) => {
    if (url != null && url[0] !== '#' && !SCHEME.test(url)) {
        const router = document.querySelector('ion-router');
        if (router) {
            if (ev != null) {
                ev.preventDefault();
            }
            return router.push(url, direction, animation);
        }
    }
    return false;
};




/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button [defaultHref]=\"'lugares/tabs/ofertas' + lugarFirebaseId\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>editar-oferta</ion-title>\n    <ion-buttons slot=\"primary\">\n      <ion-button (click)=\"onUpdateOffer()\" [disabled]=\"!form?.valid\">\n        <ion-icon name=\"checkmark\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div *ngIf=\"isLoading\" class=\"ion-padding ion-text-center\">\n    <ion-spinner color=\"primary\"></ion-spinner>\n  </div>\n\n  <form [formGroup]=\"form\" *ngIf=\"!isLoading\">\n    <ion-grid>\n      <ion-row>\n        <ion-col size-sm=\"6\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Titulo</ion-label>\n            <ion-input type=\"text\" autocomplete autocorrect formControlName=\"titulo\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size-sm=\"6\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Descripcion</ion-label>\n            <ion-textarea rows=\"3\" formControlName=\"descripcion\"></ion-textarea>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-row *ngIf=\"!form.get('descripcion').valid && form.get('descripcion').touched\">\n        <ion-col size-sm=\"6\" offset-sm=\"3\">\n          <ion-item>\n            <p>Descripcion debe ser menor que 180 caracteres y mayor a 0</p>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n  \n\n</ion-content>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.html":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"lugares/tabs/ofertas\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>nueva-oferta</ion-title>\n    <ion-buttons slot=\"primary\">\n      <ion-button (click)=\"onCreateOffer()\" [disabled]=\"!form.valid\">\n        <ion-icon name=\"checkmark\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content> \n  <form [formGroup]=\"form\">\n\n    <ion-grid>\n      <ion-row>\n        <ion-col size-md=\"6\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Titulo</ion-label>\n            <ion-input type=\"text\" autocomplete autocorrect formControlName=\"titulo\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size-md=\"6\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Descripcion</ion-label>\n            <ion-textarea rows=\"3\" formControlName=\"descripcion\"></ion-textarea>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-row *ngIf=\"!form.get('descripcion').valid && form.get('descripcion').touched\">\n        <ion-col size-md=\"6\" offset-sm=\"3\">\n          <p>Descripcion debe ser menor que 180 caracteres</p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size-sm=\"3\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Precio</ion-label>\n            <ion-input type=\"number\" formControlName=\"precio\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size-sm=\"3\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Desde</ion-label>\n            <ion-datetime min=\"2020-10-20\" max=\"2020-10-31\" formControlName=\"desde\"></ion-datetime>\n          </ion-item>\n        </ion-col>\n        <ion-col size-sm=\"3\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Hasta</ion-label>\n            <ion-datetime min=\"2020-10-20\" max=\"2020-10-31\" formControlName=\"hasta\"></ion-datetime>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size-sm=\"3\" offset-sm=\"3\">\n          <app-location-picker\n          (ubicacionSelected)=\"onUbicacionSeleccionada($event)\"></app-location-picker>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n\n</ion-content>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.html":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.html ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/lugares/tabs/ofertas\"></ion-back-button>\n    </ion-buttons>\n    <ion-title *ngIf=\"isLoading\" >Cargando...</ion-title>\n    <ion-title *ngIf=\"!isLoading\" >{{lugar.titulo}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion padding ion-text-center\">\n  <ion-spinner *ngIf=\"isLoading\" color=\"primry\"></ion-spinner>\n  <ion-button\n    *ngIf=\"!isLoading\"\n    class=\"ion-margin\"\n    color=\"primary\"\n    [routerLink]=\"['/', 'lugares', 'tabs', 'ofertas', 'edit', lugar.firebaseId]\"\n  >\n    Editar\n  </ion-button>\n</ion-content>");

/***/ }),

/***/ "./src/app/lugares/lugar.model.ts":
/*!****************************************!*\
  !*** ./src/app/lugares/lugar.model.ts ***!
  \****************************************/
/*! exports provided: Lugar */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Lugar", function() { return Lugar; });
class Lugar {
    constructor(id, titulo, descripcion, imageUrl, precio, disponibleDesde, disponibleHasta, usuarioId, firebaseId, ubicacion) {
        this.id = id;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.imageUrl = imageUrl;
        this.precio = precio;
        this.disponibleDesde = disponibleDesde;
        this.disponibleHasta = disponibleHasta;
        this.usuarioId = usuarioId;
        this.firebaseId = firebaseId;
        this.ubicacion = ubicacion;
    }
}


/***/ }),

/***/ "./src/app/lugares/lugares.service.ts":
/*!********************************************!*\
  !*** ./src/app/lugares/lugares.service.ts ***!
  \********************************************/
/*! exports provided: LugaresService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LugaresService", function() { return LugaresService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _login_login_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../login/login.service */ "./src/app/login/login.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _lugar_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./lugar.model */ "./src/app/lugares/lugar.model.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");







let LugaresService = class LugaresService {
    constructor(loginService, http) {
        this.loginService = loginService;
        this.http = http;
        this._lugares = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"]([
        /*new Lugar( 1, 'Quinta Gonzales', 'Quinta con excelente ubicacion', 'https://img10.naventcdn.com/avisos/18/00/52/30/54/90/1200x1200/63671397.jpg', 1200, new Date('2020-10-01'), new Date('2021-01-01'), 1),
       
        new Lugar(2,'Depto. Las Torres','Apartamento con excelente ubicacion','https://img10.naventcdn.com/avisos/18/00/53/55/97/00/720x532/144271181.jpg',2400, new Date('2020-10-01'), new Date('2021-01-01'), 1),
    
        new Lugar(3,'Cumbres Elite', 'Apartamento con excelente ubicacion', 'https://cf.bstatic.com/images/hotel/max1024x768/174/174836075.jpg',1800, new Date('2020-10-01'), new Date('2021-01-01'), 1),
       
        new Lugar(11,'Quinta Gonzales','Apartamento con excelente ubicacion', 'https://img10.naventcdn.com/avisos/18/00/52/30/54/90/1200x1200/63671397.jpg',1200, new Date('2020-10-01'), new Date('2021-01-01'), 1),
       
        new Lugar(12,'Depto. Las Torres', 'Apartamento con excelente ubicacion','https://img10.naventcdn.com/avisos/18/00/53/55/97/00/720x532/144271181.jpg',2400, new Date('2020-10-01'), new Date('2021-01-01'), 1),
        
        new Lugar(13,'Cumbres Elite','Apartamento con excelente ubicacion','https://cf.bstatic.com/images/hotel/max1024x768/174/174836075.jpg',1800, new Date('2020-10-01'), new Date('2021-01-01'), 1),
    
        new Lugar(21,'Quinta Gonzales','Apartamento con excelente ubicacion','https://img10.naventcdn.com/avisos/18/00/52/30/54/90/1200x1200/63671397.jpg',1200, new Date('2020-10-01'), new Date('2021-01-01'), 1),
    
        new Lugar(22,'Depto. Las Torres','Apartamento con excelente ubicacion','https://img10.naventcdn.com/avisos/18/00/53/55/97/00/720x532/144271181.jpg',2400, new Date('2020-10-01'), new Date('2021-01-01'), 1),
        
        new Lugar(23,'Cumbres Elite','Apartamento con excelente ubicacion','https://cf.bstatic.com/images/hotel/max1024x768/174/174836075.jpg',1800, new Date('2020-10-01'), new Date('2021-01-01'), 1),
        
        new Lugar(31,'Quinta Gonzales','Apartamento con excelente ubicacion','https://img10.naventcdn.com/avisos/18/00/52/30/54/90/1200x1200/63671397.jpg',1200, new Date('2020-10-01'), new Date('2021-01-01'), 1),
    
        new Lugar(32,'Depto. Las Torres','Apartamento con excelente ubicacion','https://img10.naventcdn.com/avisos/18/00/53/55/97/00/720x532/144271181.jpg',2400, new Date('2020-10-01'), new Date('2021-01-01'), 1),
    
        new Lugar( 33, 'Cumbres Elite', 'Apartamento con excelente ubicacion', 'https://cf.bstatic.com/images/hotel/max1024x768/174/174836075.jpg', 1800, new Date('2020-10-01'), new Date('2021-01-01'), 1),
      */
        ]);
    }
    get lugares() {
        return this._lugares.asObservable();
    }
    fetchLugares() {
        return this.http.get('https://practica10-5b315.firebaseio.com/ofertas-lugares.json').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(dta => {
            const lugares = [];
            for (const key in dta) {
                if (dta.hasOwnProperty(key)) {
                    lugares.push(
                    //dta
                    new _lugar_model__WEBPACK_IMPORTED_MODULE_3__["Lugar"](dta[key].id, dta[key].titulo, dta[key].descripcion, dta[key].imageUrl, dta[key].precio, new Date(dta[key].disponibleDesde), new Date(dta[key].disponibleHasta), dta[key].usuarioId, key, dta[key].ubicacion));
                }
            }
            return lugares;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])(lugares => {
            this._lugares.next(lugares);
        }));
    }
    getLugar(firebaseId) {
        return this.http.get(`https://proyecto10-390e5.firebaseio.com//ofertas-lugares/${firebaseId}.json`).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(dta => {
            return new _lugar_model__WEBPACK_IMPORTED_MODULE_3__["Lugar"](dta.id, dta.titulo, dta.descripcion, dta.imageUrl, dta.precio, new Date(dta.disponibleDesde), new Date(dta.disponibleHasta), dta.usuarioId, firebaseId, dta.ubicacion);
        }));
    }
    addLugar(titulo, descripcion, precio, disponibleDesde, disponibleHasta, ubicacion) {
        const newLugar = new _lugar_model__WEBPACK_IMPORTED_MODULE_3__["Lugar"](Math.random(), titulo, descripcion, "https://img10.naventcdn.com/avisos/18/00/53/55/97/00/720x532/144271181.jpg", precio, disponibleDesde, disponibleHasta, this.loginService.usuarioId, "", ubicacion);
        this.http.post('https://proyecto10-390e5.firebaseio.com//ofertas-lugares.json', Object.assign(Object.assign({}, newLugar), { firebaseId: null })).subscribe(data => {
            console.log(data);
            return this._lugares.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1)).subscribe(lugares => {
                this._lugares.next(lugares.concat(newLugar));
            });
        });
    }
    updateLugar(lugarId, titulo, descripcion) {
        let nuevosLugares;
        return this.lugares.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(lugares => {
            if (!lugares || lugares.length <= 0) {
                return this.fetchLugares();
            }
            else {
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(lugares);
            }
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(lugares => {
            const index = lugares.findIndex(lu => lu.firebaseId === lugarId);
            nuevosLugares = [...lugares];
            const old = nuevosLugares[index];
            nuevosLugares[index] = new _lugar_model__WEBPACK_IMPORTED_MODULE_3__["Lugar"](old.id, titulo, descripcion, old.imageUrl, old.precio, old.disponibleDesde, old.disponibleHasta, old.usuarioId, '', old.ubicacion);
            return this.http.put(`https://proyecto10-390e5.firebaseio.com//ofertas-lugares/${lugarId}.json`, Object.assign({}, nuevosLugares[index]));
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])(() => {
            this._lugares.next(nuevosLugares);
        }));
    }
};
LugaresService.ctorParameters = () => [
    { type: _login_login_service__WEBPACK_IMPORTED_MODULE_1__["LoginService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"] }
];
LugaresService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: "root",
    })
], LugaresService);



/***/ }),

/***/ "./src/app/lugares/ofertas/editar-oferta/editar-oferta-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/lugares/ofertas/editar-oferta/editar-oferta-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: EditarOfertaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarOfertaPageRoutingModule", function() { return EditarOfertaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _editar_oferta_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./editar-oferta.page */ "./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.ts");




const routes = [
    {
        path: '',
        component: _editar_oferta_page__WEBPACK_IMPORTED_MODULE_3__["EditarOfertaPage"]
    }
];
let EditarOfertaPageRoutingModule = class EditarOfertaPageRoutingModule {
};
EditarOfertaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EditarOfertaPageRoutingModule);



/***/ }),

/***/ "./src/app/lugares/ofertas/editar-oferta/editar-oferta.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/lugares/ofertas/editar-oferta/editar-oferta.module.ts ***!
  \***********************************************************************/
/*! exports provided: EditarOfertaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarOfertaPageModule", function() { return EditarOfertaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _editar_oferta_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./editar-oferta-routing.module */ "./src/app/lugares/ofertas/editar-oferta/editar-oferta-routing.module.ts");
/* harmony import */ var _editar_oferta_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./editar-oferta.page */ "./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.ts");







let EditarOfertaPageModule = class EditarOfertaPageModule {
};
EditarOfertaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            //FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _editar_oferta_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditarOfertaPageRoutingModule"]
        ],
        declarations: [_editar_oferta_page__WEBPACK_IMPORTED_MODULE_6__["EditarOfertaPage"]]
    })
], EditarOfertaPageModule);



/***/ }),

/***/ "./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2x1Z2FyZXMvb2ZlcnRhcy9lZGl0YXItb2ZlcnRhL2VkaXRhci1vZmVydGEucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.ts ***!
  \*********************************************************************/
/*! exports provided: EditarOfertaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarOfertaPage", function() { return EditarOfertaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _lugares_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../lugares.service */ "./src/app/lugares/lugares.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");






let EditarOfertaPage = class EditarOfertaPage {
    constructor(route, lugarService, navCtrl, loadingCtrl, router, alertCtrl) {
        this.route = route;
        this.lugarService = lugarService;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.isLoading = false;
    }
    ngOnInit() {
        this.route.paramMap.subscribe(param => {
            if (!param.has('lugarId')) {
                this.navCtrl.navigateBack('lugares/tabs/ofertas');
                return;
            }
            this.lugarFirebaseId = param.get('lugarId');
            this.isLoading = true;
            this.lugarSub =
                this.lugarService.getLugar(param.get('lugarId')).subscribe(lugar => {
                    this.lugar = lugar;
                    this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                        titulo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.lugar.titulo, {
                            updateOn: 'blur', validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
                        }),
                        descripcion: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.lugar.descripcion, {
                            updateOn: 'blur', validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(180)]
                        })
                    });
                    this.isLoading = false;
                }, error => {
                    this.alertCtrl.create({
                        header: 'Error',
                        message: 'Error al obtener el lugar !',
                        buttons: [
                            {
                                text: 'Ok', handler: () => {
                                    this.router.navigate(['lugares/tabs/busqueda']);
                                }
                            }
                        ]
                    }).then(alertEl => {
                        alertEl.present();
                    });
                });
        });
    }
    ngOnDestroy() {
        if (this.lugarSub) {
            this.lugarSub.unsubscribe();
        }
    }
    onUpdateOffer() {
        if (!this.form.valid) {
            return;
        }
        this.loadingCtrl.create({
            message: 'Actualizando lugar ...'
        }).then(loadEl => {
            loadEl.present();
            this.lugarService.updateLugar(this.lugar.firebaseId, this.form.value.titulo, this.form.value.descripcion).subscribe(() => {
                loadEl.dismiss();
                this.form.reset();
                this.router.navigate(['/lugares/tabs/ofertas']);
            });
        });
    }
};
EditarOfertaPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _lugares_service__WEBPACK_IMPORTED_MODULE_2__["LugaresService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] }
];
EditarOfertaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-editar-oferta',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./editar-oferta.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./editar-oferta.page.scss */ "./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.scss")).default]
    })
], EditarOfertaPage);



/***/ }),

/***/ "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/lugares/ofertas/nueva-oferta/nueva-oferta-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: NuevaOfertaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NuevaOfertaPageRoutingModule", function() { return NuevaOfertaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _nueva_oferta_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./nueva-oferta.page */ "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.ts");




const routes = [
    {
        path: '',
        component: _nueva_oferta_page__WEBPACK_IMPORTED_MODULE_3__["NuevaOfertaPage"]
    }
];
let NuevaOfertaPageRoutingModule = class NuevaOfertaPageRoutingModule {
};
NuevaOfertaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], NuevaOfertaPageRoutingModule);



/***/ }),

/***/ "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.module.ts ***!
  \*********************************************************************/
/*! exports provided: NuevaOfertaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NuevaOfertaPageModule", function() { return NuevaOfertaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _nueva_oferta_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./nueva-oferta-routing.module */ "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta-routing.module.ts");
/* harmony import */ var _nueva_oferta_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./nueva-oferta.page */ "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.ts");








let NuevaOfertaPageModule = class NuevaOfertaPageModule {
};
NuevaOfertaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            //FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _nueva_oferta_routing_module__WEBPACK_IMPORTED_MODULE_6__["NuevaOfertaPageRoutingModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]
        ],
        declarations: [_nueva_oferta_page__WEBPACK_IMPORTED_MODULE_7__["NuevaOfertaPage"]]
    })
], NuevaOfertaPageModule);



/***/ }),

/***/ "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2x1Z2FyZXMvb2ZlcnRhcy9udWV2YS1vZmVydGEvbnVldmEtb2ZlcnRhLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.ts ***!
  \*******************************************************************/
/*! exports provided: NuevaOfertaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NuevaOfertaPage", function() { return NuevaOfertaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _lugares_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../lugares.service */ "./src/app/lugares/lugares.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");





let NuevaOfertaPage = class NuevaOfertaPage {
    constructor(lugaresService, router) {
        this.lugaresService = lugaresService;
        this.router = router;
    }
    ngOnInit() {
        this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            titulo: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                updateOn: 'blur', validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
            }),
            descripcion: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                updateOn: 'blur', validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(180)]
            }),
            precio: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                updateOn: 'blur', validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].min(1)]
            }),
            desde: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                updateOn: 'blur', validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
            }),
            hasta: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                updateOn: 'blur', validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
            }),
            ubicacion: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
            })
        });
    }
    onCreateOffer() {
        if (!this.form.valid) {
            return;
        }
        this.lugaresService.addLugar(this.form.value.titulo, this.form.value.descripcion, +this.form.value.precio, new Date(this.form.value.desde), new Date(this.form.value.hasta), this.form.value.ubicacion);
        this.form.reset();
        this.router.navigate(['lugares/tabs/ofertas']);
    }
    onUbicacionSeleccionada(ubicacion) {
        this.form.patchValue({ ubicacion: ubicacion });
    }
};
NuevaOfertaPage.ctorParameters = () => [
    { type: _lugares_service__WEBPACK_IMPORTED_MODULE_1__["LugaresService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
NuevaOfertaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-nueva-oferta',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./nueva-oferta.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./nueva-oferta.page.scss */ "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.scss")).default]
    })
], NuevaOfertaPage);



/***/ }),

/***/ "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta-routing.module.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/lugares/ofertas/reservar-oferta/reservar-oferta-routing.module.ts ***!
  \***********************************************************************************/
/*! exports provided: ReservarOfertaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReservarOfertaPageRoutingModule", function() { return ReservarOfertaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _reservar_oferta_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./reservar-oferta.page */ "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.ts");




const routes = [
    {
        path: '',
        component: _reservar_oferta_page__WEBPACK_IMPORTED_MODULE_3__["ReservarOfertaPage"]
    }
];
let ReservarOfertaPageRoutingModule = class ReservarOfertaPageRoutingModule {
};
ReservarOfertaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ReservarOfertaPageRoutingModule);



/***/ }),

/***/ "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.module.ts ***!
  \***************************************************************************/
/*! exports provided: ReservarOfertaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReservarOfertaPageModule", function() { return ReservarOfertaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _reservar_oferta_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./reservar-oferta-routing.module */ "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta-routing.module.ts");
/* harmony import */ var _reservar_oferta_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./reservar-oferta.page */ "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.ts");







let ReservarOfertaPageModule = class ReservarOfertaPageModule {
};
ReservarOfertaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _reservar_oferta_routing_module__WEBPACK_IMPORTED_MODULE_5__["ReservarOfertaPageRoutingModule"]
        ],
        declarations: [_reservar_oferta_page__WEBPACK_IMPORTED_MODULE_6__["ReservarOfertaPage"]]
    })
], ReservarOfertaPageModule);



/***/ }),

/***/ "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.scss ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2x1Z2FyZXMvb2ZlcnRhcy9yZXNlcnZhci1vZmVydGEvcmVzZXJ2YXItb2ZlcnRhLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.ts ***!
  \*************************************************************************/
/*! exports provided: ReservarOfertaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReservarOfertaPage", function() { return ReservarOfertaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _lugares_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../lugares.service */ "./src/app/lugares/lugares.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");





let ReservarOfertaPage = class ReservarOfertaPage {
    constructor(route, navCtrl, lugaresService) {
        this.route = route;
        this.navCtrl = navCtrl;
        this.lugaresService = lugaresService;
        this.isLoading = false;
    }
    ngOnInit() {
        this.route.paramMap.subscribe((paramMap) => {
            if (!paramMap.has("lugarId")) {
                this.navCtrl.navigateBack("/lugares/tabs/ofertas");
                return;
            }
            this.isLoading = true;
            this.lugarSub = this.lugaresService.getLugar(paramMap.get("lugarId")).subscribe(lugares => {
                this.lugar = lugares;
                this.isLoading = false;
            });
        });
    }
    ngOnDestroy() {
        if (this.lugarSub) {
            this.lugarSub.unsubscribe();
        }
    }
};
ReservarOfertaPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _lugares_service__WEBPACK_IMPORTED_MODULE_1__["LugaresService"] }
];
ReservarOfertaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: "app-reservar-oferta",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./reservar-oferta.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./reservar-oferta.page.scss */ "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.scss")).default]
    })
], ReservarOfertaPage);



/***/ })

}]);
//# sourceMappingURL=common-es2015.js.map