(function () {
  function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

  function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

  function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

  function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"], {
    /***/
    "./node_modules/@ionic/core/dist/esm/button-active-5da929d4.js":
    /*!*********************************************************************!*\
      !*** ./node_modules/@ionic/core/dist/esm/button-active-5da929d4.js ***!
      \*********************************************************************/

    /*! exports provided: c */

    /***/
    function node_modulesIonicCoreDistEsmButtonActive5da929d4Js(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "c", function () {
        return createButtonActiveGesture;
      });
      /* harmony import */


      var _index_92848855_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./index-92848855.js */
      "./node_modules/@ionic/core/dist/esm/index-92848855.js");
      /* harmony import */


      var _index_eea61379_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./index-eea61379.js */
      "./node_modules/@ionic/core/dist/esm/index-eea61379.js");
      /* harmony import */


      var _haptic_7b8ba70a_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./haptic-7b8ba70a.js */
      "./node_modules/@ionic/core/dist/esm/haptic-7b8ba70a.js");

      var createButtonActiveGesture = function createButtonActiveGesture(el, isButton) {
        var currentTouchedButton;
        var initialTouchedButton;

        var activateButtonAtPoint = function activateButtonAtPoint(x, y, hapticFeedbackFn) {
          if (typeof document === 'undefined') {
            return;
          }

          var target = document.elementFromPoint(x, y);

          if (!target || !isButton(target)) {
            clearActiveButton();
            return;
          }

          if (target !== currentTouchedButton) {
            clearActiveButton();
            setActiveButton(target, hapticFeedbackFn);
          }
        };

        var setActiveButton = function setActiveButton(button, hapticFeedbackFn) {
          currentTouchedButton = button;

          if (!initialTouchedButton) {
            initialTouchedButton = currentTouchedButton;
          }

          var buttonToModify = currentTouchedButton;
          Object(_index_92848855_js__WEBPACK_IMPORTED_MODULE_0__["c"])(function () {
            return buttonToModify.classList.add('ion-activated');
          });
          hapticFeedbackFn();
        };

        var clearActiveButton = function clearActiveButton() {
          var dispatchClick = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

          if (!currentTouchedButton) {
            return;
          }

          var buttonToModify = currentTouchedButton;
          Object(_index_92848855_js__WEBPACK_IMPORTED_MODULE_0__["c"])(function () {
            return buttonToModify.classList.remove('ion-activated');
          });
          /**
           * Clicking on one button, but releasing on another button
           * does not dispatch a click event in browsers, so we
           * need to do it manually here. Some browsers will
           * dispatch a click if clicking on one button, dragging over
           * another button, and releasing on the original button. In that
           * case, we need to make sure we do not cause a double click there.
           */

          if (dispatchClick && initialTouchedButton !== currentTouchedButton) {
            currentTouchedButton.click();
          }

          currentTouchedButton = undefined;
        };

        return Object(_index_eea61379_js__WEBPACK_IMPORTED_MODULE_1__["createGesture"])({
          el: el,
          gestureName: 'buttonActiveDrag',
          threshold: 0,
          onStart: function onStart(ev) {
            return activateButtonAtPoint(ev.currentX, ev.currentY, _haptic_7b8ba70a_js__WEBPACK_IMPORTED_MODULE_2__["a"]);
          },
          onMove: function onMove(ev) {
            return activateButtonAtPoint(ev.currentX, ev.currentY, _haptic_7b8ba70a_js__WEBPACK_IMPORTED_MODULE_2__["b"]);
          },
          onEnd: function onEnd() {
            clearActiveButton(true);
            Object(_haptic_7b8ba70a_js__WEBPACK_IMPORTED_MODULE_2__["h"])();
            initialTouchedButton = undefined;
          }
        });
      };
      /***/

    },

    /***/
    "./node_modules/@ionic/core/dist/esm/framework-delegate-d1eb6504.js":
    /*!**************************************************************************!*\
      !*** ./node_modules/@ionic/core/dist/esm/framework-delegate-d1eb6504.js ***!
      \**************************************************************************/

    /*! exports provided: a, d */

    /***/
    function node_modulesIonicCoreDistEsmFrameworkDelegateD1eb6504Js(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "a", function () {
        return attachComponent;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "d", function () {
        return detachComponent;
      });

      var attachComponent = /*#__PURE__*/function () {
        var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(delegate, container, component, cssClasses, componentProps) {
          var el;
          return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  if (!delegate) {
                    _context.next = 2;
                    break;
                  }

                  return _context.abrupt("return", delegate.attachViewToDom(container, component, componentProps, cssClasses));

                case 2:
                  if (!(typeof component !== 'string' && !(component instanceof HTMLElement))) {
                    _context.next = 4;
                    break;
                  }

                  throw new Error('framework delegate is missing');

                case 4:
                  el = typeof component === 'string' ? container.ownerDocument && container.ownerDocument.createElement(component) : component;

                  if (cssClasses) {
                    cssClasses.forEach(function (c) {
                      return el.classList.add(c);
                    });
                  }

                  if (componentProps) {
                    Object.assign(el, componentProps);
                  }

                  container.appendChild(el);

                  if (!el.componentOnReady) {
                    _context.next = 11;
                    break;
                  }

                  _context.next = 11;
                  return el.componentOnReady();

                case 11:
                  return _context.abrupt("return", el);

                case 12:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee);
        }));

        return function attachComponent(_x, _x2, _x3, _x4, _x5) {
          return _ref.apply(this, arguments);
        };
      }();

      var detachComponent = function detachComponent(delegate, element) {
        if (element) {
          if (delegate) {
            var container = element.parentElement;
            return delegate.removeViewFromDom(container, element);
          }

          element.remove();
        }

        return Promise.resolve();
      };
      /***/

    },

    /***/
    "./node_modules/@ionic/core/dist/esm/haptic-7b8ba70a.js":
    /*!**************************************************************!*\
      !*** ./node_modules/@ionic/core/dist/esm/haptic-7b8ba70a.js ***!
      \**************************************************************/

    /*! exports provided: a, b, c, d, h */

    /***/
    function node_modulesIonicCoreDistEsmHaptic7b8ba70aJs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "a", function () {
        return hapticSelectionStart;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "b", function () {
        return hapticSelectionChanged;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "c", function () {
        return hapticSelection;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "d", function () {
        return hapticImpact;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "h", function () {
        return hapticSelectionEnd;
      });

      var HapticEngine = {
        getEngine: function getEngine() {
          var win = window;
          return win.TapticEngine || win.Capacitor && win.Capacitor.isPluginAvailable('Haptics') && win.Capacitor.Plugins.Haptics;
        },
        available: function available() {
          return !!this.getEngine();
        },
        isCordova: function isCordova() {
          return !!window.TapticEngine;
        },
        isCapacitor: function isCapacitor() {
          var win = window;
          return !!win.Capacitor;
        },
        impact: function impact(options) {
          var engine = this.getEngine();

          if (!engine) {
            return;
          }

          var style = this.isCapacitor() ? options.style.toUpperCase() : options.style;
          engine.impact({
            style: style
          });
        },
        notification: function notification(options) {
          var engine = this.getEngine();

          if (!engine) {
            return;
          }

          var style = this.isCapacitor() ? options.style.toUpperCase() : options.style;
          engine.notification({
            style: style
          });
        },
        selection: function selection() {
          this.impact({
            style: 'light'
          });
        },
        selectionStart: function selectionStart() {
          var engine = this.getEngine();

          if (!engine) {
            return;
          }

          if (this.isCapacitor()) {
            engine.selectionStart();
          } else {
            engine.gestureSelectionStart();
          }
        },
        selectionChanged: function selectionChanged() {
          var engine = this.getEngine();

          if (!engine) {
            return;
          }

          if (this.isCapacitor()) {
            engine.selectionChanged();
          } else {
            engine.gestureSelectionChanged();
          }
        },
        selectionEnd: function selectionEnd() {
          var engine = this.getEngine();

          if (!engine) {
            return;
          }

          if (this.isCapacitor()) {
            engine.selectionEnd();
          } else {
            engine.gestureSelectionEnd();
          }
        }
      };
      /**
       * Trigger a selection changed haptic event. Good for one-time events
       * (not for gestures)
       */

      var hapticSelection = function hapticSelection() {
        HapticEngine.selection();
      };
      /**
       * Tell the haptic engine that a gesture for a selection change is starting.
       */


      var hapticSelectionStart = function hapticSelectionStart() {
        HapticEngine.selectionStart();
      };
      /**
       * Tell the haptic engine that a selection changed during a gesture.
       */


      var hapticSelectionChanged = function hapticSelectionChanged() {
        HapticEngine.selectionChanged();
      };
      /**
       * Tell the haptic engine we are done with a gesture. This needs to be
       * called lest resources are not properly recycled.
       */


      var hapticSelectionEnd = function hapticSelectionEnd() {
        HapticEngine.selectionEnd();
      };
      /**
       * Use this to indicate success/failure/warning to the user.
       * options should be of the type `{ style: 'light' }` (or `medium`/`heavy`)
       */


      var hapticImpact = function hapticImpact(options) {
        HapticEngine.impact(options);
      };
      /***/

    },

    /***/
    "./node_modules/@ionic/core/dist/esm/spinner-configs-c78e170e.js":
    /*!***********************************************************************!*\
      !*** ./node_modules/@ionic/core/dist/esm/spinner-configs-c78e170e.js ***!
      \***********************************************************************/

    /*! exports provided: S */

    /***/
    function node_modulesIonicCoreDistEsmSpinnerConfigsC78e170eJs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "S", function () {
        return SPINNERS;
      });

      var spinners = {
        'bubbles': {
          dur: 1000,
          circles: 9,
          fn: function fn(dur, index, total) {
            var animationDelay = "".concat(dur * index / total - dur, "ms");
            var angle = 2 * Math.PI * index / total;
            return {
              r: 5,
              style: {
                'top': "".concat(9 * Math.sin(angle), "px"),
                'left': "".concat(9 * Math.cos(angle), "px"),
                'animation-delay': animationDelay
              }
            };
          }
        },
        'circles': {
          dur: 1000,
          circles: 8,
          fn: function fn(dur, index, total) {
            var step = index / total;
            var animationDelay = "".concat(dur * step - dur, "ms");
            var angle = 2 * Math.PI * step;
            return {
              r: 5,
              style: {
                'top': "".concat(9 * Math.sin(angle), "px"),
                'left': "".concat(9 * Math.cos(angle), "px"),
                'animation-delay': animationDelay
              }
            };
          }
        },
        'circular': {
          dur: 1400,
          elmDuration: true,
          circles: 1,
          fn: function fn() {
            return {
              r: 20,
              cx: 48,
              cy: 48,
              fill: 'none',
              viewBox: '24 24 48 48',
              transform: 'translate(0,0)',
              style: {}
            };
          }
        },
        'crescent': {
          dur: 750,
          circles: 1,
          fn: function fn() {
            return {
              r: 26,
              style: {}
            };
          }
        },
        'dots': {
          dur: 750,
          circles: 3,
          fn: function fn(_, index) {
            var animationDelay = -(110 * index) + 'ms';
            return {
              r: 6,
              style: {
                'left': "".concat(9 - 9 * index, "px"),
                'animation-delay': animationDelay
              }
            };
          }
        },
        'lines': {
          dur: 1000,
          lines: 12,
          fn: function fn(dur, index, total) {
            var transform = "rotate(".concat(30 * index + (index < 6 ? 180 : -180), "deg)");
            var animationDelay = "".concat(dur * index / total - dur, "ms");
            return {
              y1: 17,
              y2: 29,
              style: {
                'transform': transform,
                'animation-delay': animationDelay
              }
            };
          }
        },
        'lines-small': {
          dur: 1000,
          lines: 12,
          fn: function fn(dur, index, total) {
            var transform = "rotate(".concat(30 * index + (index < 6 ? 180 : -180), "deg)");
            var animationDelay = "".concat(dur * index / total - dur, "ms");
            return {
              y1: 12,
              y2: 20,
              style: {
                'transform': transform,
                'animation-delay': animationDelay
              }
            };
          }
        }
      };
      var SPINNERS = spinners;
      /***/
    },

    /***/
    "./node_modules/@ionic/core/dist/esm/theme-5641d27f.js":
    /*!*************************************************************!*\
      !*** ./node_modules/@ionic/core/dist/esm/theme-5641d27f.js ***!
      \*************************************************************/

    /*! exports provided: c, g, h, o */

    /***/
    function node_modulesIonicCoreDistEsmTheme5641d27fJs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "c", function () {
        return createColorClasses;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "g", function () {
        return getClassMap;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "h", function () {
        return hostContext;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "o", function () {
        return openURL;
      });

      var hostContext = function hostContext(selector, el) {
        return el.closest(selector) !== null;
      };
      /**
       * Create the mode and color classes for the component based on the classes passed in
       */


      var createColorClasses = function createColorClasses(color, cssClassMap) {
        return typeof color === 'string' && color.length > 0 ? Object.assign(_defineProperty({
          'ion-color': true
        }, "ion-color-".concat(color), true), cssClassMap) : cssClassMap;
      };

      var getClassList = function getClassList(classes) {
        if (classes !== undefined) {
          var array = Array.isArray(classes) ? classes : classes.split(' ');
          return array.filter(function (c) {
            return c != null;
          }).map(function (c) {
            return c.trim();
          }).filter(function (c) {
            return c !== '';
          });
        }

        return [];
      };

      var getClassMap = function getClassMap(classes) {
        var map = {};
        getClassList(classes).forEach(function (c) {
          return map[c] = true;
        });
        return map;
      };

      var SCHEME = /^[a-z][a-z0-9+\-.]*:/;

      var openURL = /*#__PURE__*/function () {
        var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(url, ev, direction, animation) {
          var router;
          return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  if (!(url != null && url[0] !== '#' && !SCHEME.test(url))) {
                    _context2.next = 5;
                    break;
                  }

                  router = document.querySelector('ion-router');

                  if (!router) {
                    _context2.next = 5;
                    break;
                  }

                  if (ev != null) {
                    ev.preventDefault();
                  }

                  return _context2.abrupt("return", router.push(url, direction, animation));

                case 5:
                  return _context2.abrupt("return", false);

                case 6:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2);
        }));

        return function openURL(_x6, _x7, _x8, _x9) {
          return _ref2.apply(this, arguments);
        };
      }();
      /***/

    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.html":
    /*!*************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.html ***!
      \*************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppLugaresOfertasEditarOfertaEditarOfertaPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button [defaultHref]=\"'lugares/tabs/ofertas' + lugarFirebaseId\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>editar-oferta</ion-title>\n    <ion-buttons slot=\"primary\">\n      <ion-button (click)=\"onUpdateOffer()\" [disabled]=\"!form?.valid\">\n        <ion-icon name=\"checkmark\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div *ngIf=\"isLoading\" class=\"ion-padding ion-text-center\">\n    <ion-spinner color=\"primary\"></ion-spinner>\n  </div>\n\n  <form [formGroup]=\"form\" *ngIf=\"!isLoading\">\n    <ion-grid>\n      <ion-row>\n        <ion-col size-sm=\"6\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Titulo</ion-label>\n            <ion-input type=\"text\" autocomplete autocorrect formControlName=\"titulo\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size-sm=\"6\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Descripcion</ion-label>\n            <ion-textarea rows=\"3\" formControlName=\"descripcion\"></ion-textarea>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-row *ngIf=\"!form.get('descripcion').valid && form.get('descripcion').touched\">\n        <ion-col size-sm=\"6\" offset-sm=\"3\">\n          <ion-item>\n            <p>Descripcion debe ser menor que 180 caracteres y mayor a 0</p>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n  \n\n</ion-content>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.html":
    /*!***********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.html ***!
      \***********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppLugaresOfertasNuevaOfertaNuevaOfertaPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"lugares/tabs/ofertas\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>nueva-oferta</ion-title>\n    <ion-buttons slot=\"primary\">\n      <ion-button (click)=\"onCreateOffer()\" [disabled]=\"!form.valid\">\n        <ion-icon name=\"checkmark\" slot=\"icon-only\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content> \n  <form [formGroup]=\"form\">\n\n    <ion-grid>\n      <ion-row>\n        <ion-col size-md=\"6\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Titulo</ion-label>\n            <ion-input type=\"text\" autocomplete autocorrect formControlName=\"titulo\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size-md=\"6\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Descripcion</ion-label>\n            <ion-textarea rows=\"3\" formControlName=\"descripcion\"></ion-textarea>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-row *ngIf=\"!form.get('descripcion').valid && form.get('descripcion').touched\">\n        <ion-col size-md=\"6\" offset-sm=\"3\">\n          <p>Descripcion debe ser menor que 180 caracteres</p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size-sm=\"3\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Precio</ion-label>\n            <ion-input type=\"number\" formControlName=\"precio\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size-sm=\"3\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Desde</ion-label>\n            <ion-datetime min=\"2020-10-20\" max=\"2020-10-31\" formControlName=\"desde\"></ion-datetime>\n          </ion-item>\n        </ion-col>\n        <ion-col size-sm=\"3\" offset-sm=\"3\">\n          <ion-item>\n            <ion-label position=\"floating\">Hasta</ion-label>\n            <ion-datetime min=\"2020-10-20\" max=\"2020-10-31\" formControlName=\"hasta\"></ion-datetime>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size-sm=\"3\" offset-sm=\"3\">\n          <app-location-picker\n          (ubicacionSelected)=\"onUbicacionSeleccionada($event)\"></app-location-picker>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n\n</ion-content>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.html":
    /*!*****************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.html ***!
      \*****************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppLugaresOfertasReservarOfertaReservarOfertaPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/lugares/tabs/ofertas\"></ion-back-button>\n    </ion-buttons>\n    <ion-title *ngIf=\"isLoading\" >Cargando...</ion-title>\n    <ion-title *ngIf=\"!isLoading\" >{{lugar.titulo}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion padding ion-text-center\">\n  <ion-spinner *ngIf=\"isLoading\" color=\"primry\"></ion-spinner>\n  <ion-button\n    *ngIf=\"!isLoading\"\n    class=\"ion-margin\"\n    color=\"primary\"\n    [routerLink]=\"['/', 'lugares', 'tabs', 'ofertas', 'edit', lugar.firebaseId]\"\n  >\n    Editar\n  </ion-button>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/lugares/lugar.model.ts":
    /*!****************************************!*\
      !*** ./src/app/lugares/lugar.model.ts ***!
      \****************************************/

    /*! exports provided: Lugar */

    /***/
    function srcAppLugaresLugarModelTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Lugar", function () {
        return Lugar;
      });

      var Lugar = function Lugar(id, titulo, descripcion, imageUrl, precio, disponibleDesde, disponibleHasta, usuarioId, firebaseId, ubicacion) {
        _classCallCheck(this, Lugar);

        this.id = id;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.imageUrl = imageUrl;
        this.precio = precio;
        this.disponibleDesde = disponibleDesde;
        this.disponibleHasta = disponibleHasta;
        this.usuarioId = usuarioId;
        this.firebaseId = firebaseId;
        this.ubicacion = ubicacion;
      };
      /***/

    },

    /***/
    "./src/app/lugares/lugares.service.ts":
    /*!********************************************!*\
      !*** ./src/app/lugares/lugares.service.ts ***!
      \********************************************/

    /*! exports provided: LugaresService */

    /***/
    function srcAppLugaresLugaresServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LugaresService", function () {
        return LugaresService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _login_login_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../login/login.service */
      "./src/app/login/login.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _lugar_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./lugar.model */
      "./src/app/lugares/lugar.model.ts");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var LugaresService = /*#__PURE__*/function () {
        function LugaresService(loginService, http) {
          _classCallCheck(this, LugaresService);

          this.loginService = loginService;
          this.http = http;
          this._lugares = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"]([
            /*new Lugar( 1, 'Quinta Gonzales', 'Quinta con excelente ubicacion', 'https://img10.naventcdn.com/avisos/18/00/52/30/54/90/1200x1200/63671397.jpg', 1200, new Date('2020-10-01'), new Date('2021-01-01'), 1),
            
            new Lugar(2,'Depto. Las Torres','Apartamento con excelente ubicacion','https://img10.naventcdn.com/avisos/18/00/53/55/97/00/720x532/144271181.jpg',2400, new Date('2020-10-01'), new Date('2021-01-01'), 1),
                  new Lugar(3,'Cumbres Elite', 'Apartamento con excelente ubicacion', 'https://cf.bstatic.com/images/hotel/max1024x768/174/174836075.jpg',1800, new Date('2020-10-01'), new Date('2021-01-01'), 1),
            
            new Lugar(11,'Quinta Gonzales','Apartamento con excelente ubicacion', 'https://img10.naventcdn.com/avisos/18/00/52/30/54/90/1200x1200/63671397.jpg',1200, new Date('2020-10-01'), new Date('2021-01-01'), 1),
            
            new Lugar(12,'Depto. Las Torres', 'Apartamento con excelente ubicacion','https://img10.naventcdn.com/avisos/18/00/53/55/97/00/720x532/144271181.jpg',2400, new Date('2020-10-01'), new Date('2021-01-01'), 1),
            
            new Lugar(13,'Cumbres Elite','Apartamento con excelente ubicacion','https://cf.bstatic.com/images/hotel/max1024x768/174/174836075.jpg',1800, new Date('2020-10-01'), new Date('2021-01-01'), 1),
                  new Lugar(21,'Quinta Gonzales','Apartamento con excelente ubicacion','https://img10.naventcdn.com/avisos/18/00/52/30/54/90/1200x1200/63671397.jpg',1200, new Date('2020-10-01'), new Date('2021-01-01'), 1),
                  new Lugar(22,'Depto. Las Torres','Apartamento con excelente ubicacion','https://img10.naventcdn.com/avisos/18/00/53/55/97/00/720x532/144271181.jpg',2400, new Date('2020-10-01'), new Date('2021-01-01'), 1),
            
            new Lugar(23,'Cumbres Elite','Apartamento con excelente ubicacion','https://cf.bstatic.com/images/hotel/max1024x768/174/174836075.jpg',1800, new Date('2020-10-01'), new Date('2021-01-01'), 1),
            
            new Lugar(31,'Quinta Gonzales','Apartamento con excelente ubicacion','https://img10.naventcdn.com/avisos/18/00/52/30/54/90/1200x1200/63671397.jpg',1200, new Date('2020-10-01'), new Date('2021-01-01'), 1),
                  new Lugar(32,'Depto. Las Torres','Apartamento con excelente ubicacion','https://img10.naventcdn.com/avisos/18/00/53/55/97/00/720x532/144271181.jpg',2400, new Date('2020-10-01'), new Date('2021-01-01'), 1),
                  new Lugar( 33, 'Cumbres Elite', 'Apartamento con excelente ubicacion', 'https://cf.bstatic.com/images/hotel/max1024x768/174/174836075.jpg', 1800, new Date('2020-10-01'), new Date('2021-01-01'), 1),
            */
          ]);
        }

        _createClass(LugaresService, [{
          key: "fetchLugares",
          value: function fetchLugares() {
            var _this = this;

            return this.http.get('https://practica10-5b315.firebaseio.com/ofertas-lugares.json').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (dta) {
              var lugares = [];

              for (var key in dta) {
                if (dta.hasOwnProperty(key)) {
                  lugares.push( //dta
                  new _lugar_model__WEBPACK_IMPORTED_MODULE_3__["Lugar"](dta[key].id, dta[key].titulo, dta[key].descripcion, dta[key].imageUrl, dta[key].precio, new Date(dta[key].disponibleDesde), new Date(dta[key].disponibleHasta), dta[key].usuarioId, key, dta[key].ubicacion));
                }
              }

              return lugares;
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])(function (lugares) {
              _this._lugares.next(lugares);
            }));
          }
        }, {
          key: "getLugar",
          value: function getLugar(firebaseId) {
            return this.http.get("https://proyecto10-390e5.firebaseio.com//ofertas-lugares/".concat(firebaseId, ".json")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (dta) {
              return new _lugar_model__WEBPACK_IMPORTED_MODULE_3__["Lugar"](dta.id, dta.titulo, dta.descripcion, dta.imageUrl, dta.precio, new Date(dta.disponibleDesde), new Date(dta.disponibleHasta), dta.usuarioId, firebaseId, dta.ubicacion);
            }));
          }
        }, {
          key: "addLugar",
          value: function addLugar(titulo, descripcion, precio, disponibleDesde, disponibleHasta, ubicacion) {
            var _this2 = this;

            var newLugar = new _lugar_model__WEBPACK_IMPORTED_MODULE_3__["Lugar"](Math.random(), titulo, descripcion, "https://img10.naventcdn.com/avisos/18/00/53/55/97/00/720x532/144271181.jpg", precio, disponibleDesde, disponibleHasta, this.loginService.usuarioId, "", ubicacion);
            this.http.post('https://proyecto10-390e5.firebaseio.com//ofertas-lugares.json', Object.assign(Object.assign({}, newLugar), {
              firebaseId: null
            })).subscribe(function (data) {
              console.log(data);
              return _this2._lugares.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1)).subscribe(function (lugares) {
                _this2._lugares.next(lugares.concat(newLugar));
              });
            });
          }
        }, {
          key: "updateLugar",
          value: function updateLugar(lugarId, titulo, descripcion) {
            var _this3 = this;

            var nuevosLugares;
            return this.lugares.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (lugares) {
              if (!lugares || lugares.length <= 0) {
                return _this3.fetchLugares();
              } else {
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(lugares);
              }
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (lugares) {
              var index = lugares.findIndex(function (lu) {
                return lu.firebaseId === lugarId;
              });
              nuevosLugares = _toConsumableArray(lugares);
              var old = nuevosLugares[index];
              nuevosLugares[index] = new _lugar_model__WEBPACK_IMPORTED_MODULE_3__["Lugar"](old.id, titulo, descripcion, old.imageUrl, old.precio, old.disponibleDesde, old.disponibleHasta, old.usuarioId, '', old.ubicacion);
              return _this3.http.put("https://proyecto10-390e5.firebaseio.com//ofertas-lugares/".concat(lugarId, ".json"), Object.assign({}, nuevosLugares[index]));
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])(function () {
              _this3._lugares.next(nuevosLugares);
            }));
          }
        }, {
          key: "lugares",
          get: function get() {
            return this._lugares.asObservable();
          }
        }]);

        return LugaresService;
      }();

      LugaresService.ctorParameters = function () {
        return [{
          type: _login_login_service__WEBPACK_IMPORTED_MODULE_1__["LoginService"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"]
        }];
      };

      LugaresService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: "root"
      })], LugaresService);
      /***/
    },

    /***/
    "./src/app/lugares/ofertas/editar-oferta/editar-oferta-routing.module.ts":
    /*!*******************************************************************************!*\
      !*** ./src/app/lugares/ofertas/editar-oferta/editar-oferta-routing.module.ts ***!
      \*******************************************************************************/

    /*! exports provided: EditarOfertaPageRoutingModule */

    /***/
    function srcAppLugaresOfertasEditarOfertaEditarOfertaRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditarOfertaPageRoutingModule", function () {
        return EditarOfertaPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _editar_oferta_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./editar-oferta.page */
      "./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.ts");

      var routes = [{
        path: '',
        component: _editar_oferta_page__WEBPACK_IMPORTED_MODULE_3__["EditarOfertaPage"]
      }];

      var EditarOfertaPageRoutingModule = function EditarOfertaPageRoutingModule() {
        _classCallCheck(this, EditarOfertaPageRoutingModule);
      };

      EditarOfertaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], EditarOfertaPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/lugares/ofertas/editar-oferta/editar-oferta.module.ts":
    /*!***********************************************************************!*\
      !*** ./src/app/lugares/ofertas/editar-oferta/editar-oferta.module.ts ***!
      \***********************************************************************/

    /*! exports provided: EditarOfertaPageModule */

    /***/
    function srcAppLugaresOfertasEditarOfertaEditarOfertaModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditarOfertaPageModule", function () {
        return EditarOfertaPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _editar_oferta_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./editar-oferta-routing.module */
      "./src/app/lugares/ofertas/editar-oferta/editar-oferta-routing.module.ts");
      /* harmony import */


      var _editar_oferta_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./editar-oferta.page */
      "./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.ts");

      var EditarOfertaPageModule = function EditarOfertaPageModule() {
        _classCallCheck(this, EditarOfertaPageModule);
      };

      EditarOfertaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], //FormsModule,
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _editar_oferta_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditarOfertaPageRoutingModule"]],
        declarations: [_editar_oferta_page__WEBPACK_IMPORTED_MODULE_6__["EditarOfertaPage"]]
      })], EditarOfertaPageModule);
      /***/
    },

    /***/
    "./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.scss":
    /*!***********************************************************************!*\
      !*** ./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.scss ***!
      \***********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppLugaresOfertasEditarOfertaEditarOfertaPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2x1Z2FyZXMvb2ZlcnRhcy9lZGl0YXItb2ZlcnRhL2VkaXRhci1vZmVydGEucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.ts ***!
      \*********************************************************************/

    /*! exports provided: EditarOfertaPage */

    /***/
    function srcAppLugaresOfertasEditarOfertaEditarOfertaPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditarOfertaPage", function () {
        return EditarOfertaPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _lugares_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./../../lugares.service */
      "./src/app/lugares/lugares.service.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var EditarOfertaPage = /*#__PURE__*/function () {
        function EditarOfertaPage(route, lugarService, navCtrl, loadingCtrl, router, alertCtrl) {
          _classCallCheck(this, EditarOfertaPage);

          this.route = route;
          this.lugarService = lugarService;
          this.navCtrl = navCtrl;
          this.loadingCtrl = loadingCtrl;
          this.router = router;
          this.alertCtrl = alertCtrl;
          this.isLoading = false;
        }

        _createClass(EditarOfertaPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this4 = this;

            this.route.paramMap.subscribe(function (param) {
              if (!param.has('lugarId')) {
                _this4.navCtrl.navigateBack('lugares/tabs/ofertas');

                return;
              }

              _this4.lugarFirebaseId = param.get('lugarId');
              _this4.isLoading = true;
              _this4.lugarSub = _this4.lugarService.getLugar(param.get('lugarId')).subscribe(function (lugar) {
                _this4.lugar = lugar;
                _this4.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                  titulo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this4.lugar.titulo, {
                    updateOn: 'blur',
                    validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
                  }),
                  descripcion: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this4.lugar.descripcion, {
                    updateOn: 'blur',
                    validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(180)]
                  })
                });
                _this4.isLoading = false;
              }, function (error) {
                _this4.alertCtrl.create({
                  header: 'Error',
                  message: 'Error al obtener el lugar !',
                  buttons: [{
                    text: 'Ok',
                    handler: function handler() {
                      _this4.router.navigate(['lugares/tabs/busqueda']);
                    }
                  }]
                }).then(function (alertEl) {
                  alertEl.present();
                });
              });
            });
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            if (this.lugarSub) {
              this.lugarSub.unsubscribe();
            }
          }
        }, {
          key: "onUpdateOffer",
          value: function onUpdateOffer() {
            var _this5 = this;

            if (!this.form.valid) {
              return;
            }

            this.loadingCtrl.create({
              message: 'Actualizando lugar ...'
            }).then(function (loadEl) {
              loadEl.present();

              _this5.lugarService.updateLugar(_this5.lugar.firebaseId, _this5.form.value.titulo, _this5.form.value.descripcion).subscribe(function () {
                loadEl.dismiss();

                _this5.form.reset();

                _this5.router.navigate(['/lugares/tabs/ofertas']);
              });
            });
          }
        }]);

        return EditarOfertaPage;
      }();

      EditarOfertaPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
        }, {
          type: _lugares_service__WEBPACK_IMPORTED_MODULE_2__["LugaresService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"]
        }];
      };

      EditarOfertaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-editar-oferta',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./editar-oferta.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./editar-oferta.page.scss */
        "./src/app/lugares/ofertas/editar-oferta/editar-oferta.page.scss"))["default"]]
      })], EditarOfertaPage);
      /***/
    },

    /***/
    "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta-routing.module.ts":
    /*!*****************************************************************************!*\
      !*** ./src/app/lugares/ofertas/nueva-oferta/nueva-oferta-routing.module.ts ***!
      \*****************************************************************************/

    /*! exports provided: NuevaOfertaPageRoutingModule */

    /***/
    function srcAppLugaresOfertasNuevaOfertaNuevaOfertaRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NuevaOfertaPageRoutingModule", function () {
        return NuevaOfertaPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _nueva_oferta_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./nueva-oferta.page */
      "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.ts");

      var routes = [{
        path: '',
        component: _nueva_oferta_page__WEBPACK_IMPORTED_MODULE_3__["NuevaOfertaPage"]
      }];

      var NuevaOfertaPageRoutingModule = function NuevaOfertaPageRoutingModule() {
        _classCallCheck(this, NuevaOfertaPageRoutingModule);
      };

      NuevaOfertaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], NuevaOfertaPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.module.ts ***!
      \*********************************************************************/

    /*! exports provided: NuevaOfertaPageModule */

    /***/
    function srcAppLugaresOfertasNuevaOfertaNuevaOfertaModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NuevaOfertaPageModule", function () {
        return NuevaOfertaPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./../../../shared/shared.module */
      "./src/app/shared/shared.module.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _nueva_oferta_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./nueva-oferta-routing.module */
      "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta-routing.module.ts");
      /* harmony import */


      var _nueva_oferta_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./nueva-oferta.page */
      "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.ts");

      var NuevaOfertaPageModule = function NuevaOfertaPageModule() {
        _classCallCheck(this, NuevaOfertaPageModule);
      };

      NuevaOfertaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], //FormsModule,
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _nueva_oferta_routing_module__WEBPACK_IMPORTED_MODULE_6__["NuevaOfertaPageRoutingModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]],
        declarations: [_nueva_oferta_page__WEBPACK_IMPORTED_MODULE_7__["NuevaOfertaPage"]]
      })], NuevaOfertaPageModule);
      /***/
    },

    /***/
    "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.scss":
    /*!*********************************************************************!*\
      !*** ./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.scss ***!
      \*********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppLugaresOfertasNuevaOfertaNuevaOfertaPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2x1Z2FyZXMvb2ZlcnRhcy9udWV2YS1vZmVydGEvbnVldmEtb2ZlcnRhLnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.ts ***!
      \*******************************************************************/

    /*! exports provided: NuevaOfertaPage */

    /***/
    function srcAppLugaresOfertasNuevaOfertaNuevaOfertaPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NuevaOfertaPage", function () {
        return NuevaOfertaPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _lugares_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../../lugares.service */
      "./src/app/lugares/lugares.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var NuevaOfertaPage = /*#__PURE__*/function () {
        function NuevaOfertaPage(lugaresService, router) {
          _classCallCheck(this, NuevaOfertaPage);

          this.lugaresService = lugaresService;
          this.router = router;
        }

        _createClass(NuevaOfertaPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
              titulo: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                updateOn: 'blur',
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
              }),
              descripcion: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                updateOn: 'blur',
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].maxLength(180)]
              }),
              precio: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                updateOn: 'blur',
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].min(1)]
              }),
              desde: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                updateOn: 'blur',
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
              }),
              hasta: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                updateOn: 'blur',
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
              }),
              ubicacion: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, {
                validators: [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
              })
            });
          }
        }, {
          key: "onCreateOffer",
          value: function onCreateOffer() {
            if (!this.form.valid) {
              return;
            }

            this.lugaresService.addLugar(this.form.value.titulo, this.form.value.descripcion, +this.form.value.precio, new Date(this.form.value.desde), new Date(this.form.value.hasta), this.form.value.ubicacion);
            this.form.reset();
            this.router.navigate(['lugares/tabs/ofertas']);
          }
        }, {
          key: "onUbicacionSeleccionada",
          value: function onUbicacionSeleccionada(ubicacion) {
            this.form.patchValue({
              ubicacion: ubicacion
            });
          }
        }]);

        return NuevaOfertaPage;
      }();

      NuevaOfertaPage.ctorParameters = function () {
        return [{
          type: _lugares_service__WEBPACK_IMPORTED_MODULE_1__["LugaresService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }];
      };

      NuevaOfertaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-nueva-oferta',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./nueva-oferta.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./nueva-oferta.page.scss */
        "./src/app/lugares/ofertas/nueva-oferta/nueva-oferta.page.scss"))["default"]]
      })], NuevaOfertaPage);
      /***/
    },

    /***/
    "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta-routing.module.ts":
    /*!***********************************************************************************!*\
      !*** ./src/app/lugares/ofertas/reservar-oferta/reservar-oferta-routing.module.ts ***!
      \***********************************************************************************/

    /*! exports provided: ReservarOfertaPageRoutingModule */

    /***/
    function srcAppLugaresOfertasReservarOfertaReservarOfertaRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ReservarOfertaPageRoutingModule", function () {
        return ReservarOfertaPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _reservar_oferta_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./reservar-oferta.page */
      "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.ts");

      var routes = [{
        path: '',
        component: _reservar_oferta_page__WEBPACK_IMPORTED_MODULE_3__["ReservarOfertaPage"]
      }];

      var ReservarOfertaPageRoutingModule = function ReservarOfertaPageRoutingModule() {
        _classCallCheck(this, ReservarOfertaPageRoutingModule);
      };

      ReservarOfertaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ReservarOfertaPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.module.ts":
    /*!***************************************************************************!*\
      !*** ./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.module.ts ***!
      \***************************************************************************/

    /*! exports provided: ReservarOfertaPageModule */

    /***/
    function srcAppLugaresOfertasReservarOfertaReservarOfertaModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ReservarOfertaPageModule", function () {
        return ReservarOfertaPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _reservar_oferta_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./reservar-oferta-routing.module */
      "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta-routing.module.ts");
      /* harmony import */


      var _reservar_oferta_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./reservar-oferta.page */
      "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.ts");

      var ReservarOfertaPageModule = function ReservarOfertaPageModule() {
        _classCallCheck(this, ReservarOfertaPageModule);
      };

      ReservarOfertaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _reservar_oferta_routing_module__WEBPACK_IMPORTED_MODULE_5__["ReservarOfertaPageRoutingModule"]],
        declarations: [_reservar_oferta_page__WEBPACK_IMPORTED_MODULE_6__["ReservarOfertaPage"]]
      })], ReservarOfertaPageModule);
      /***/
    },

    /***/
    "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.scss":
    /*!***************************************************************************!*\
      !*** ./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.scss ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppLugaresOfertasReservarOfertaReservarOfertaPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2x1Z2FyZXMvb2ZlcnRhcy9yZXNlcnZhci1vZmVydGEvcmVzZXJ2YXItb2ZlcnRhLnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.ts":
    /*!*************************************************************************!*\
      !*** ./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.ts ***!
      \*************************************************************************/

    /*! exports provided: ReservarOfertaPage */

    /***/
    function srcAppLugaresOfertasReservarOfertaReservarOfertaPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ReservarOfertaPage", function () {
        return ReservarOfertaPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _lugares_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../../lugares.service */
      "./src/app/lugares/lugares.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var ReservarOfertaPage = /*#__PURE__*/function () {
        function ReservarOfertaPage(route, navCtrl, lugaresService) {
          _classCallCheck(this, ReservarOfertaPage);

          this.route = route;
          this.navCtrl = navCtrl;
          this.lugaresService = lugaresService;
          this.isLoading = false;
        }

        _createClass(ReservarOfertaPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this6 = this;

            this.route.paramMap.subscribe(function (paramMap) {
              if (!paramMap.has("lugarId")) {
                _this6.navCtrl.navigateBack("/lugares/tabs/ofertas");

                return;
              }

              _this6.isLoading = true;
              _this6.lugarSub = _this6.lugaresService.getLugar(paramMap.get("lugarId")).subscribe(function (lugares) {
                _this6.lugar = lugares;
                _this6.isLoading = false;
              });
            });
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            if (this.lugarSub) {
              this.lugarSub.unsubscribe();
            }
          }
        }]);

        return ReservarOfertaPage;
      }();

      ReservarOfertaPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
        }, {
          type: _lugares_service__WEBPACK_IMPORTED_MODULE_1__["LugaresService"]
        }];
      };

      ReservarOfertaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: "app-reservar-oferta",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./reservar-oferta.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./reservar-oferta.page.scss */
        "./src/app/lugares/ofertas/reservar-oferta/reservar-oferta.page.scss"))["default"]]
      })], ReservarOfertaPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=common-es5.js.map